﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
   public partial class Form1 : Form
   {
      public Form1()
      {
         InitializeComponent();
      }

      private void button1_Click(object sender, EventArgs e)
      {
         Bitmap spike1 = new Bitmap(panel1.BackgroundImage);
         for (int i = 0; i < spike1.Width; i++)
         {
            for (int lcv = 0; lcv < spike1.Height; lcv++)
            {
               Color Temp = spike1.GetPixel(lcv, i);
               String TempSpikeName = Temp.ToString();
               if (TempSpikeName.Equals("Color [A=255, R=255, G=255, B=255]"))
                  textBox1.AppendText("   ");
               else
                  textBox1.AppendText(" B ");               
            }
            textBox1.AppendText(Environment.NewLine);            
         }        
      }

      private void button2_Click(object sender, EventArgs e)
      {
         
      }
   }
}
