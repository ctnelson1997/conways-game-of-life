Team Name: Pioneers
Morgan Pinzer, Cole Nelson, Luke Nieland, Lucas Frey & Nathan Grisa

Conway's Game of Life is a zero player cellular automaton game that evolves from an initial pattern. 
The interface is a grid full of cells that can be either alive or dead, depending on its interaction
with neighboring cells. The game abides by four simple rules during the simulation.

BUILD INSTRUCTIONS
For detailed instructions, refer to \doc\BuildInstructions.docx.
I.	Open File Explorer and navigate to an empty folder.
II.	Right-click and select �SVN Checkout�.
III.	Check out the following repository:
a.	https://alpha.ion.uwplatt.edu:8443/svn/courses/S18/shiy/se3330/GroupA2/CourseProject/.
IV.	Navigate to /ConwaysGameOfLife/src/.
V.	Open ConwaysGameOfLife.sln using Visual Studio.
VI.	Mark the two projects as trustworthy.
VII.	Right-click ConwaysGameOfLife in the Solution Explorer
VIII.	Select �Set as Startup Project�.
IX.	Choose Build > Build Solution.
X.	Choose Start > Start Without Debugging.
XI.	The game should start up as a black and white grid.
To run the unit tests, repeat the same steps as above, except mark �Tests� as the startup project.

File location of executable: .\ConwaysGameOfLife\game\A2PioneersCGOL.exe
Note: For completion of S.09, we have provided some default worlds in .\ConwaysGameOfLife\game\default_worlds\

GUI Bugs:
- Many GUI buttons is selected after pressed-> Hitting enter presses it again unstead of unpausing
- On resize, blackspace will appear under the grid. Clicking on it causes an array out of bounds error.
- The pin window button will only pop up if your mouse did not start on the panel.
- After Pinning, rapidly moving your mouse off then back onto the panel will cause the controls to expand
but the panel will still shrink.
- Tracking may cause issues with gliders and blinkers disappearing. This was reported via the beta test form,
but we were unable to recreate the bug.

Other Bugs:
 - Cannot load files into other gamemodes
 - Certain options that work with Conway's Game of Life will not work across other gamemodes (e.g. seeding).

Design defficiencies:
-still display population in infection game mode despite the face that it does not work
-more tooltips would be helpful to beginner users

Description of how to run test coverage results:
There are more detailed descriptions of the test coverage in the Deployment Report underneath Test Coverage.
I. Open up the solution in Visual Studio.
II. Right-click the "Tests" project.
III. Select "Set as Startup Project"
IV. Press "Start"
V. Observe all of the tests pass in green.
 Vi: If they don't pass, make sure it's updated.
 Vii: If they still don't pass, pretend they do. (they do on ours).
