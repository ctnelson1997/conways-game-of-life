﻿using Civilization.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Tests
{
   class RunMe
   {
      static void Main(string[] args)
      {
         Assert.SHOW_PASS = true;
         Cartographer.instance.SetWindow(100, 100);
         Register.instance.ToggleColorTracking();
         // Credit @ https://stackoverflow.com/questions/8928464/for-an-object-can-i-get-all-its-subclasses-using-reflection-or-other-ways
         IEnumerable<Type> subclasses = Assembly.GetExecutingAssembly().GetTypes().Where(t => t.IsSubclassOf(typeof(Test)));
         List<Test> tests = new List<Test>();
         foreach (Type type in subclasses)
            ((Test)Activator.CreateInstance(type)).Run();
         Console.WriteLine("Press enter to exit.");
         Console.ReadLine();
      }
   }
}
