﻿using Civilization.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
   public abstract class Test
   {
      public Test()
      {
         Cartographer.instance.Reset();
      }
      public abstract void Run();
   }
}
