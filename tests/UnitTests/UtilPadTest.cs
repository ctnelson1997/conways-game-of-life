﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tests;
using Civilization.Models;

namespace tests.UnitTests
{
   class UtilPadTest : Test
   {
      public override void Run()
      {
         string[,] arr = new string[2,2];
         arr[0, 0] = "hello";
         arr[0, 1] = "world";
         arr[1, 0] = "from";
         arr[1, 1] = "pioneers";
         string[,] paddedArr = Util.Pad<string>(arr, "empty");
         Assert.IsTrue(paddedArr[0, 0].Equals("empty") &&
            paddedArr[0, 1].Equals("empty") &&
            paddedArr[0, 2].Equals("empty") &&
            paddedArr[0, 3].Equals("empty") &&
            paddedArr[1, 0].Equals("empty") &&
            paddedArr[2, 0].Equals("empty") &&
            paddedArr[3, 0].Equals("empty") &&
            paddedArr[3, 0].Equals("empty") &&
            paddedArr[3, 0].Equals("empty") &&
            paddedArr[3, 0].Equals("empty") &&
            paddedArr[0, 3].Equals("empty") &&
            paddedArr[0, 3].Equals("empty") &&
            paddedArr[0, 3].Equals("empty"), "Padding 2x2 -> 4x4");
      }
   }
}
