﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civilization.Models;

namespace Tests.Tests
{
   class ConwaysRule3 : Test
   {
      public override void Run()
      {
         FourNeighbors();
         Cartographer.instance.Reset();
         FiveNeighbors();
      }

      private void FourNeighbors()
      {
         Tile centerTile = Cartographer.instance.map[1][1];
         Tile tile0 = Cartographer.instance.map[0][1];
         Tile tile1 = Cartographer.instance.map[1][0];
         Tile tile2 = Cartographer.instance.map[2][1];
         Tile tile3 = Cartographer.instance.map[2][0];
         centerTile.cell.state = Cell.State.ALIVE;
         tile0.cell.state = Cell.State.ALIVE;
         tile1.cell.state = Cell.State.ALIVE;
         tile2.cell.state = Cell.State.ALIVE;
         tile3.cell.state = Cell.State.ALIVE;
         Register.instance.Update();
         Assert.IsTrue(centerTile.cell.state.Equals(Cell.State.DEAD), "Dead with 4 Neighbors");
      }

      private void FiveNeighbors()
      {
         Tile centerTile = Cartographer.instance.map[1][1];
         Tile tile0 = Cartographer.instance.map[0][1];
         Tile tile1 = Cartographer.instance.map[1][0];
         Tile tile2 = Cartographer.instance.map[2][1];
         Tile tile3 = Cartographer.instance.map[2][0];
         Tile tile4 = Cartographer.instance.map[2][2];
         centerTile.cell.state = Cell.State.ALIVE;
         tile0.cell.state = Cell.State.ALIVE;
         tile1.cell.state = Cell.State.ALIVE;
         tile2.cell.state = Cell.State.ALIVE;
         tile3.cell.state = Cell.State.ALIVE;
         tile4.cell.state = Cell.State.ALIVE;
         Register.instance.Update();
         Assert.IsTrue(centerTile.cell.state.Equals(Cell.State.DEAD), "Dead with 5 Neighbors");
      }
   }
}
