﻿using Civilization.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Tests;

namespace Tests.Tests
{
   class OrganismTracking : Test
   {
      public override void Run()
      {
         TestBeehives();
         TestBlinkers();
         TestGliders();
         TestCombo();
      }

      private void TestBeehives()
      {
         Cartographer.instance.Reset();
         Organism.BEEHIVE.Spawn(10, 10);
         Register.instance.Update();

         Assert.IsTrue(
               Register.instance.aliveOrganisms[Organism.BEEHIVE] == 1
            && Register.instance.aliveOrganisms[Organism.BLINKER] == 0
            && Register.instance.aliveOrganisms[Organism.GLIDER] == 0
            , "1 Beehive Spawned!");
      }

      private void TestBlinkers()
      {
         Cartographer.instance.Reset();
         Organism.BLINKER.Spawn(12, 10);
         Organism.BLINKER.Spawn(17, 10);
         Organism.BLINKER.Spawn(4, 7);
         Register.instance.Update();

         Assert.IsTrue(
               Register.instance.aliveOrganisms[Organism.BEEHIVE] == 0
            && Register.instance.aliveOrganisms[Organism.BLINKER] == 3
            && Register.instance.aliveOrganisms[Organism.GLIDER] == 0
            , "3 Blinkers Spawned!");
      }

      private void TestGliders()
      {
         Cartographer.instance.Reset();
         Organism.GLIDER.Spawn(17, 10);
         Organism.GLIDER.Spawn(4, 7);
         Register.instance.Update();

         Assert.IsTrue(
               Register.instance.aliveOrganisms[Organism.BEEHIVE] == 0
            && Register.instance.aliveOrganisms[Organism.BLINKER] == 0
            && Register.instance.aliveOrganisms[Organism.GLIDER] == 2
            , "2 Gliders Spawned!");
      }

      private void TestCombo()
      {
         Cartographer.instance.Reset();
         Organism.BLINKER.Spawn(42, 10);
         Organism.BLINKER.Spawn(47, 10);
         Organism.BLINKER.Spawn(20, 8);
         Organism.GLIDER.Spawn(32, 20);
         Organism.GLIDER.Spawn(20, 32);
         Organism.BEEHIVE.Spawn(14, 4);
         Register.instance.Update();

         Assert.IsTrue(
               Register.instance.aliveOrganisms[Organism.BEEHIVE] == 1
            && Register.instance.aliveOrganisms[Organism.BLINKER] == 3
            && Register.instance.aliveOrganisms[Organism.GLIDER] == 2
            , "3 Blinkers, 2 Gliders, 1 Beehive Spawned!");
      }
   }
}
