﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civilization.Models;

namespace Tests.Tests
{
   class ConwaysRule4 : Test
   {
      public override void Run()
      {
         TwoNeighbors();
         Cartographer.instance.Reset();
         ThreeNeighbors();
         Cartographer.instance.Reset();
         FourNeighbors();
      }

      private void TwoNeighbors()
      {
         Tile centerTile = Cartographer.instance.map[1][1];
         Tile tile0 = Cartographer.instance.map[0][1];
         Tile tile1 = Cartographer.instance.map[1][0];
         centerTile.cell.state = Cell.State.DEAD;
         tile0.cell.state = Cell.State.ALIVE;
         tile1.cell.state = Cell.State.ALIVE;
         Register.instance.Update();
         Assert.IsTrue(centerTile.cell.state.Equals(Cell.State.DEAD), "Dead with 2 Neighbors");
      }

      private void ThreeNeighbors()
      {
         Tile centerTile = Cartographer.instance.map[1][1];
         Tile tile0 = Cartographer.instance.map[0][1];
         Tile tile1 = Cartographer.instance.map[1][0];
         Tile tile2 = Cartographer.instance.map[1][2];
         centerTile.cell.state = Cell.State.DEAD;
         tile0.cell.state = Cell.State.ALIVE;
         tile1.cell.state = Cell.State.ALIVE;
         tile2.cell.state = Cell.State.ALIVE;
         Register.instance.Update();
         Assert.IsTrue(centerTile.cell.state.Equals(Cell.State.ALIVE), "Alive with 3 Neighbors");
      }

      private void FourNeighbors()
      {
         Tile centerTile = Cartographer.instance.map[1][1];
         Tile tile0 = Cartographer.instance.map[0][1];
         Tile tile1 = Cartographer.instance.map[1][0];
         Tile tile2 = Cartographer.instance.map[1][2];
         Tile tile3 = Cartographer.instance.map[2][2];
         centerTile.cell.state = Cell.State.DEAD;
         tile0.cell.state = Cell.State.ALIVE;
         tile1.cell.state = Cell.State.ALIVE;
         tile2.cell.state = Cell.State.ALIVE;
         tile3.cell.state = Cell.State.ALIVE;
         Register.instance.Update();
         Assert.IsTrue(centerTile.cell.state.Equals(Cell.State.DEAD), "Dead with 4 Neighbors");
      }
   }
}
