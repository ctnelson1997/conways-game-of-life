﻿using Civilization.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Tests;

namespace Tests.Tests
{
   class OrganismColoring : Test
   {
      public override void Run()
      {
         TestBeehive();
         TestBlinker();
         TestGlider();
      }

      private void TestBeehive()
      {
         Cartographer.instance.Reset();
         Organism.BEEHIVE.Spawn(10, 10);
         Register.instance.Update();

         Assert.IsTrue(Cartographer.instance.map[10][11].cell.color.Equals(Brushes.Goldenrod),
            "Beehive colored!");
      }

      private void TestBlinker()
      {
         Cartographer.instance.Reset();
         Organism.BLINKER.Spawn(10, 10);
         Register.instance.Update();

         Assert.IsTrue(Cartographer.instance.map[10][11].cell.color.Equals(Brushes.IndianRed),
            "Blinker colored!");
      }

      private void TestGlider()
      {
         Cartographer.instance.Reset();
         Organism.GLIDER.Spawn(10, 10);
         Register.instance.Update();

         Assert.IsTrue(Cartographer.instance.map[12][11].cell.color.Equals(Brushes.Aqua),
            "Glider colored!");
      }
   }
}
