﻿using Civilization.Models;
using Civilization.CommandPattern;
using System;
using Tests;

namespace tests.UnitTests
{
   class Undo : Test
   {
      public Cell.State[,] GLIDER_0 = new Cell.State[3, 3]
      { {Cell.State.ALIVE, Cell.State.ALIVE, Cell.State.DEAD },
        {Cell.State.DEAD, Cell.State.DEAD, Cell.State.ALIVE},
        {Cell.State.ALIVE, Cell.State.ALIVE, Cell.State.ALIVE}, };


      public override void Run()
      {
         Cartographer.instance.Reset();
         Cartographer.instance.map[0][0].cell.state = Cell.State.ALIVE;
         Cartographer.instance.map[0][1].cell.state = Cell.State.ALIVE;
         Cartographer.instance.map[0][2].cell.state = Cell.State.DEAD;
         Cartographer.instance.map[1][0].cell.state = Cell.State.DEAD;
         Cartographer.instance.map[1][1].cell.state = Cell.State.DEAD;
         Cartographer.instance.map[1][2].cell.state = Cell.State.ALIVE;
         Cartographer.instance.map[2][0].cell.state = Cell.State.DEAD;
         Cartographer.instance.map[2][1].cell.state = Cell.State.ALIVE;
         Cartographer.instance.map[2][2].cell.state = Cell.State.DEAD;
         Cell.State[,] testArr = new Cell.State[3, 3];
         testArr[0,0] = Cell.State.ALIVE;
         testArr[0,1] = Cell.State.ALIVE;
         testArr[0,2] = Cell.State.DEAD;
         testArr[1,0] = Cell.State.DEAD;
         testArr[1,1] = Cell.State.DEAD;
         testArr[1,2] = Cell.State.ALIVE;
         testArr[2,0] = Cell.State.DEAD;
         testArr[2,1] = Cell.State.ALIVE;
         testArr[2,2] = Cell.State.DEAD;
         Register.instance.Update();
         CommandInvoker.instance.Undo();
         bool temp = true;
         for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
               temp = temp && (Cartographer.instance.map[i][j].cell.state == testArr[i, j]);
         Assert.IsTrue(temp, "Undo 1 Generation");
      }
   }
}
