﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tests;

namespace tests.UnitTests
{
   class AssertTest : Test
   {
      public override void Run()
      {
         RunTrue();
         RunFalse();
      }

      public void RunTrue()
      {
         Assert.IsTrue(true, "Assertion for true asserted true");
      }

      public void RunFalse()
      {
         Assert.IsFalse(false, "Assertion for false asserted false");
      }
   }
}
