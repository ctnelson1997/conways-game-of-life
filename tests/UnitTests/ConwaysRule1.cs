﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civilization.Models;

namespace Tests.Tests
{
   class ConwaysRule1 : Test
   {
      public override void Run()
      {
         NoNeighbors();
         Cartographer.instance.Reset();
         OneNeighbor();
      }

      public void NoNeighbors()
      {
         Tile centerTile = Cartographer.instance.map[1][1];
         centerTile.cell.state = Cell.State.ALIVE;
         Register.instance.Update();
         Assert.IsTrue(centerTile.cell.state.Equals(Cell.State.DEAD), "Alive with No Neighbors");
      }

      public void OneNeighbor()
      {
         Tile centerTile = Cartographer.instance.map[1][1];
         Tile tile0 = Cartographer.instance.map[0][1];
         centerTile.cell.state = Cell.State.ALIVE;
         tile0.cell.state = Cell.State.ALIVE;
         Register.instance.Update();
         Assert.IsTrue(centerTile.cell.state.Equals(Cell.State.DEAD), "Alive with One Neighbor");
      }
   }
}
