﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tests;
using Civilization.Models;

namespace tests.UnitTests
{
   class UtilRotateTest : Test
   {
      public override void Run()
      {
         int[,] arr = new int[2,2];
         arr[0, 0] = 0;
         arr[1, 0] = 0;
         arr[0, 1] = 1;
         arr[1, 1] = 0;
         int[,] rotatedArr = new int[2, 2];
         rotatedArr[0, 0] = 0;
         rotatedArr[1, 0] = 0;
         rotatedArr[0, 1] = 0;
         rotatedArr[1, 1] = 1;
         arr = Util.RotateMatrixCounterClockwise<int>(arr);
         arr = Util.RotateMatrixCounterClockwise<int>(arr);
         arr = Util.RotateMatrixCounterClockwise<int>(arr);
         Assert.IsTrue(arr[0,0] == rotatedArr[0,0] &&
            arr[0, 1] == rotatedArr[0, 1] &&
            arr[1, 0] == rotatedArr[1, 0] &&
            arr[1, 1] == rotatedArr[1, 1], "Rotated the array");
      }
   }
}
