﻿using Civilization.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Tests;

namespace Tests.Tests
{
   class AddOrganism : Test
   {
      public override void Run()
      {
         bool IsStillGood = true;

         Organism.GLIDER.Spawn(0, 0);

         Tile tile0 = Cartographer.instance.map[0][0];
         Tile tile1 = Cartographer.instance.map[0][1];
         Tile tile2 = Cartographer.instance.map[0][2];
         Tile tile3 = Cartographer.instance.map[1][0];
         Tile tile4 = Cartographer.instance.map[1][1];
         Tile tile5 = Cartographer.instance.map[1][2];
         Tile tile6 = Cartographer.instance.map[2][0];
         Tile tile7 = Cartographer.instance.map[2][1];
         Tile tile8 = Cartographer.instance.map[2][2];

         if (tile0.cell.state == Cell.State.ALIVE)
            IsStillGood = false;
         if (tile1.cell.state == Cell.State.DEAD)
            IsStillGood = false;
         if (tile2.cell.state == Cell.State.ALIVE)
            IsStillGood = false;
         if (tile3.cell.state == Cell.State.ALIVE)
            IsStillGood = false;
         if (tile4.cell.state == Cell.State.ALIVE)
            IsStillGood = false;
         if (tile5.cell.state == Cell.State.DEAD)
            IsStillGood = false;
         if (tile6.cell.state == Cell.State.DEAD)
            IsStillGood = false;
         if (tile7.cell.state == Cell.State.DEAD)
            IsStillGood = false;
         if (tile8.cell.state == Cell.State.DEAD)
            IsStillGood = false;


         Assert.IsTrue(IsStillGood == true, "Glider Organism Added");
      }
   }
}
