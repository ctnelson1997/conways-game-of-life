﻿using Civilization.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Tests;

namespace Tests.Tests
{
   class ClearAllCells : Test
   {
      public override void Run()
      {
         bool IsStillGood = true;

         Cartographer.instance.map[67][0].ChangeColor();
         Cartographer.instance.map[0][1].ChangeColor();
         Cartographer.instance.map[43][2].ChangeColor();
         Cartographer.instance.map[23][3].ChangeColor();
         Cartographer.instance.map[88][4].ChangeColor();
         Cartographer.instance.map[7][5].ChangeColor();
         Cartographer.instance.map[6][6].ChangeColor();
         Cartographer.instance.map[2][7].ChangeColor();
         Cartographer.instance.map[1][8].ChangeColor();

         Cartographer.instance.Reset();

         if (Cartographer.instance.map[67][0].cell.state == Cell.State.ALIVE)
            IsStillGood = false;
         if (Cartographer.instance.map[0][1].cell.state == Cell.State.ALIVE)
            IsStillGood = false;
         if (Cartographer.instance.map[43][2].cell.state == Cell.State.ALIVE)
            IsStillGood = false;
         if (Cartographer.instance.map[23][3].cell.state == Cell.State.ALIVE)
            IsStillGood = false;
         if (Cartographer.instance.map[88][4].cell.state == Cell.State.ALIVE)
            IsStillGood = false;
         if (Cartographer.instance.map[7][5].cell.state == Cell.State.ALIVE)
            IsStillGood = false;
         if (Cartographer.instance.map[6][6].cell.state == Cell.State.ALIVE)
            IsStillGood = false;
         if (Cartographer.instance.map[2][7].cell.state == Cell.State.ALIVE)
            IsStillGood = false;
         if (Cartographer.instance.map[1][8].cell.state == Cell.State.ALIVE)
            IsStillGood = false;

         Assert.IsTrue(IsStillGood == true, "All Cells are Cleared");


      }
   }
}
