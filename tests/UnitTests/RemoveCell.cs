﻿using Civilization.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Tests;

namespace Tests.Tests
{
   class RemoveCell : Test
   {
      public override void Run()
      {
         Cartographer.instance.map[0][0].ChangeColor();
         Cartographer.instance.map[0][0].ChangeColor();
         Assert.IsTrue(Cartographer.instance.map[0][0].cell.color.ToString().Equals("#00FFFFFF"), "Cell removed from Grid");
      }
   }
}
