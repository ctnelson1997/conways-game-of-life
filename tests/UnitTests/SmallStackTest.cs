﻿using Civilization.CommandPattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tests;

namespace tests.UnitTests
{
   class SmallStackTest : Test
   {
      private SmallStack<String> st = new SmallStack<String>(5);

      public override void Run()
      {
         DoPushes();
         DoPops();
         DoPushOver();
         DoEmpty();
         DoFull();
      }

      public void DoPushes()
      {
         st.Push("Hello");
         st.Push("World");
         Assert.IsTrue(st.Count() == 2, "Pushed 2 items to the stack");
      }

      public void DoPops()
      {
         st.Pop();
         st.Pop();
         Assert.IsTrue(st.Count() == 0, "Popped 2 items from the stack");
      }

      public void DoPushOver()
      {
         st.Push("1");
         st.Push("2");
         st.Push("3");
         st.Push("4");
         st.Push("5");
         st.Push("6"); // pushover, should repl 1
         Assert.IsTrue(st.Pop().Equals("6"), "(1) Overflow worked correctly");
         Assert.IsTrue(st.Pop().Equals("5"), "(2) Overflow worked correctly");
         Assert.IsTrue(st.Pop().Equals("4"), "(3) Overflow worked correctly");
         Assert.IsTrue(st.Pop().Equals("3"), "(4) Overflow worked correctly");
         Assert.IsTrue(st.Pop().Equals("2"), "(5) Overflow worked correctly");
      }

      public void DoEmpty()
      {
         Assert.IsTrue(st.IsEmpty(), "Stack empty");
      }

      public void DoFull()
      {
         while(!st.IsFull())
         {
            st.Push("hello");
         }
         Assert.IsTrue(st.IsFull(), "Filled stack");
      }
   }
}
