﻿using Civilization.CommandPattern;
using Civilization.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tests;

namespace tests.UnitTests
{
   class UndoRedo : Test
   {
      public override void Run()
      {
         DoGenerations(1);
         DoGenerations(5);
         DoGenerations(10);
         DoGenerations(25);
      }

      public void DoGenerations(int numGens)
      {
         this.Populate();
         for (int i = 0; i < numGens; i++)
            Register.instance.Update();
         for (int i = 0; i < numGens; i++)
            CommandInvoker.instance.Undo();
         for (int i = 0; i < numGens; i++)
            CommandInvoker.instance.Redo();
         for (int i = 0; i < numGens; i++)
            CommandInvoker.instance.Undo();
         Assert.IsTrue(MatchesOriginal(), "Undo/Redo " + numGens + " generations.");
      }

      private void Populate()
      {
         Cartographer.instance.Reset();
         Cartographer.instance.map[0][0].cell.state = Cell.State.ALIVE;
         Cartographer.instance.map[0][1].cell.state = Cell.State.ALIVE;
         Cartographer.instance.map[0][2].cell.state = Cell.State.DEAD;
         Cartographer.instance.map[1][0].cell.state = Cell.State.DEAD;
         Cartographer.instance.map[1][1].cell.state = Cell.State.DEAD;
         Cartographer.instance.map[1][2].cell.state = Cell.State.ALIVE;
         Cartographer.instance.map[2][0].cell.state = Cell.State.DEAD;
         Cartographer.instance.map[2][1].cell.state = Cell.State.ALIVE;
         Cartographer.instance.map[2][2].cell.state = Cell.State.DEAD;
      }

      private bool MatchesOriginal()
      {
         return (Cartographer.instance.map[0][0].cell.state == Cell.State.ALIVE
                  && Cartographer.instance.map[0][1].cell.state == Cell.State.ALIVE
                  && Cartographer.instance.map[0][2].cell.state == Cell.State.DEAD
                  && Cartographer.instance.map[1][0].cell.state == Cell.State.DEAD
                  && Cartographer.instance.map[1][1].cell.state == Cell.State.DEAD
                  && Cartographer.instance.map[1][2].cell.state == Cell.State.ALIVE
                  && Cartographer.instance.map[2][0].cell.state == Cell.State.DEAD
                  && Cartographer.instance.map[2][1].cell.state == Cell.State.ALIVE
                  && Cartographer.instance.map[2][2].cell.state == Cell.State.DEAD);
      }
   }
}
