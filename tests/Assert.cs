﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
   static class Assert
   {

      public static bool SHOW_PASS = true;

      public static bool IsFalse(bool var)
      {
         return IsFalse(var, null);
      }
      public static bool IsTrue(bool var)
      {
         return IsTrue(var, null);
      }
      public static bool IsFalse(bool var, string msg)
      {
         return IsTrue(!var, msg);
      }

      public static bool IsTrue(bool var, string msg)
      {
         String testName = new StackTrace().GetFrame(1).GetMethod().DeclaringType.Name.ToUpper();
         if (var)
         {
            if (SHOW_PASS)
            {
               Console.ForegroundColor = ConsoleColor.Green;
               Console.WriteLine("Assertion PASSED in " + testName);
               if (msg == null) return true;
               Console.WriteLine(" - Additional Info: " + msg);
               Console.ForegroundColor = ConsoleColor.White;
            }
            return true;
         }
         Console.ForegroundColor = ConsoleColor.Red;
         Console.WriteLine("Assertion FAILED in " + testName);
         if (msg == null) return false;
         Console.WriteLine(" - Additional Info: " + msg);
         Console.ForegroundColor = ConsoleColor.White;
         return false;
      }
   }
}
