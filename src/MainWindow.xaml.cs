﻿//---------------------------------------------------------------------
// Group:   Pioneers
// Project: Conway's Game of Life
// Purpose: To handle UI related tasks such as displaying the grid and
//          other elements such as the text box for displaying the
//          generations and the slider to increase or decrease the 
//          speed of the generations. Also handles mouse clicking and
//          key pressing.
//---------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Civilization.Models;
using System.Windows.Threading;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Civilization.SideMenu;

namespace Civilization
{
   //---------------------------------------------------------------------
   // To handle UI related tasks such as displaying the grid and other
   // elements such as the text box for displaying the generations and the
   // slider to increase or decrease the speed of the generations. Also 
   // handles mouse clicking and key pressing.
   //---------------------------------------------------------------------
   public partial class MainWindow : Window
   {
      #region INotifyPropertyChangedData
      public event PropertyChangedEventHandler PropertyChanged;

      protected void OnPropertyChanged(string propertyName)
      {
         var handler = PropertyChanged;

         if (handler != null)
         {
            handler(this, new PropertyChangedEventArgs(propertyName));
         }
      }

      private void NotifyPropertyChanged(string str)
      {
         if (PropertyChanged != null)
         {
            PropertyChanged(this, new PropertyChangedEventArgs(str));
         }
      }
      #endregion

      private Launcher launcher;

      private MainGameView mainGameView;

      public Register register { get; private set; }
      public Scheduler schedule { get; private set; }
      public Cartographer world { get; private set; }

      // ----------------------------------------------------------------
      // Constructor to initialize window and instances of other classes.
      // ----------------------------------------------------------------
      public MainWindow()
      {
         world = Cartographer.instance;
         schedule = Scheduler.instance;
         register = Register.instance;

         InitializeComponent();

         launcher = new Launcher();

         GameContentControl.Content = launcher;

         mainGameView = new MainGameView();

         InitializeCommands();

      }

      // -----------------------------------------------------------------
      // Initializes commands for controls not defined in the base class.
      // -----------------------------------------------------------------
      public void InitializeCommands()
      {
         launcher.XAMLplayButton.Click += OnPlayButtonClick;
         mainGameView.sideMenuPanel.organismHubMenuPanel.XAMLQuitButton.Click += OnQuitButtonClicked;

      }

      // -----------------------------------------------------------------
      // Command for when the Play button is clicked. It will rotate to
      // the main game view.
      // -----------------------------------------------------------------
      public void OnPlayButtonClick(object sender, RoutedEventArgs e)
      {
         world.Clear();
         GameContentControl.Content = mainGameView;
      }

      // -----------------------------------------------------------------
      // Command for when the Play button is clicked. It will rotate to
      // the launcher.
      // -----------------------------------------------------------------
      public void OnQuitButtonClicked(object sender, RoutedEventArgs e)
      {
         world.Clear();
         GameContentControl.Content = launcher;
      }
   }
}
