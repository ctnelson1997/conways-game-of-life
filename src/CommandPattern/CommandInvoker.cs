﻿//---------------------------------------------------------------------
// Group:   Pioneers
// Project: Conway's Game of Life
// Purpose: Used for calling commands, i.e. invoking them.
//          Is a singleton, so can theoritically be used from anywhere.
//---------------------------------------------------------------------

using Civilization.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civilization.CommandPattern
{
   // -------------------------------------------------------------------
   // Used for calling commands, i.e. invoking them.
   // Is a singleton, so can theoritically be used from anywhere.
   // -------------------------------------------------------------------
   public class CommandInvoker
   {
      private SmallStack<Command> undos = new SmallStack<Command>();
      private SmallStack<Command> redos = new SmallStack<Command>();

      #region Singleton Properties
      private static CommandInvoker _instance = new CommandInvoker();
      public static CommandInvoker instance
      {
         get
         {
            return _instance;
         }
      }

      // ----------------------------------------------------------------
      // Prepares the dictionary
      // ----------------------------------------------------------------
      private CommandInvoker() { }
      #endregion

      // ----------------------------------------------------------------
      // Calls the passed command
      // ----------------------------------------------------------------
      public void Call(Command command)
      {
         undos.Push(command);
         redos.Clear();
         command.Execute();
      }

      // ----------------------------------------------------------------
      // Undoes the last command on the stack
      // ----------------------------------------------------------------
      public void Undo()
      {
         if (undos.Count() <= 0)
            return;
         Command cmd = undos.Pop();
         cmd.Undo();
         redos.Push(cmd);
      }

      // ----------------------------------------------------------------
      // Redoes the last command on the stack
      // ----------------------------------------------------------------
      public void Redo()
      {
         if (redos.Count() <= 0)
            return;
         Command cmd = redos.Pop();
         cmd.Redo();
         undos.Push(cmd);
      }

      // ----------------------------------------------------------------
      // Clears both of the stacks
      // ----------------------------------------------------------------
      public void Clear()
      {
         undos.Clear();
         redos.Clear();
      }
   }
}
