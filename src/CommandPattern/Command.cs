﻿//---------------------------------------------------------------------
// Group:   Pioneers
// Project: Conway's Game of Life
// Purpose: Abstract class for all commands. Asserts all commands
//          have three parts: execute, undo, and redo. Used in
//          the command pattern.
//---------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// -------------------------------------------------------------------
// Abstract class for all commands. Asserts all commands have three
// parts: execute, undo, and redo. Used in the command pattern.
// -------------------------------------------------------------------
namespace Civilization.Models
{
   public abstract class Command
   {
      // ----------------------------------------------------------------
      // Executes the command
      // ----------------------------------------------------------------
      public abstract void Execute();

      // ----------------------------------------------------------------
      // Undoes the command
      // ----------------------------------------------------------------
      public abstract void Undo();

      // ----------------------------------------------------------------
      // Redoes the command
      // ----------------------------------------------------------------
      public abstract void Redo();
   }
}
