﻿//---------------------------------------------------------------------
// Group:   Pioneers
// Project: Conway's Game of Life
// Purpose: Abstract Data Type developed by the Pioneers of UW-P.
//          Used for a stack which has a limited size, yet requires
//          the update of the oldest content. If the stack is full,
//          it replaces the oldest content with what is being pushed.
//---------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civilization.CommandPattern
{
   // -------------------------------------------------------------------
   // Abstract Data Type developed by the Pioneers of UW-P.
   // Used for a stack which has a limited size, yet requires
   // the update of the oldest content. If the stack is full,
   // it replaces the oldest content with what is being pushed.
   // -------------------------------------------------------------------
   public class SmallStack<T> where T : class
   {
      private readonly int MAX_SIZE;
      private const int DEFAULT_MAX = 250;

      private Node top = null;
      private Node bot = null;
      private int size = 0;

      // -------------------------------------------------------------------
      // Default constructor, sets to the default size of 250 -- "small"
      // -------------------------------------------------------------------
      public SmallStack() : this(DEFAULT_MAX) { }

      // -------------------------------------------------------------------
      // Parameterized constructor for the command, takes in size.
      // -------------------------------------------------------------------
      public SmallStack(int maxSize)
      {
         if (maxSize <= 0)
            maxSize = DEFAULT_MAX;
         this.MAX_SIZE = maxSize;
      }

      // -------------------------------------------------------------------
      // Pushes the object to the stack, deleting the oldest and
      // replacing it with what is being pushed.
      // -------------------------------------------------------------------
      public void Push(T obj)
      {
         Node inc = new Node(obj, null, null);
         if (this.IsFull())
         {
            bot = bot.next;
            bot.prev = null;
            size--;
         }
         if (this.IsEmpty())
         {
            top = inc;
            bot = inc;
            size++;
            return;
         }
         top.next = inc;
         inc.prev = top;
         top = inc;
         size++;
      }

      // -------------------------------------------------------------------
      // Pops from the top of the stack.
      // -------------------------------------------------------------------
      public T Pop()
      {
         if (size == 0)
            return null;
         size--;
         T temp = top.data;
         if (size == 0)
         {
            top = null;
            bot = null;
         }
         else
         {
            top = top.prev;
            top.next = null;
         }
         return temp;
      }

      // -------------------------------------------------------------------
      // Returns if the stack is full
      // -------------------------------------------------------------------
      public bool IsFull()
      {
         return (size == MAX_SIZE);
      }

      // -------------------------------------------------------------------
      // Returns if the stack is empty
      // -------------------------------------------------------------------
      public bool IsEmpty()
      {
         return (size == 0);
      }

      // -------------------------------------------------------------------
      // Returns the size of the stack
      // -------------------------------------------------------------------
      public int Count()
      {
         return this.size;
      }

      // -------------------------------------------------------------------
      // "Clears" the stack
      // -------------------------------------------------------------------
      public void Clear()
      {
         top = null;
         bot = null;
         size = 0;
      }

      // -------------------------------------------------------------------
      // A small stack uses a doubly-linked list in the background.
      // This requires nodes, pointing frontwards and backwards.
      // A node holds these pointers and the actual data.
      // -------------------------------------------------------------------
      private class Node
      {
         public readonly T data;
         public Node next;
         public Node prev;

         // -------------------------------------------------------------------
         // Constructor requires all aspects of a node.
         // -------------------------------------------------------------------
         public Node(T data, Node prev, Node next)
         {
            this.data = data;
            this.prev = prev;
            this.next = next;
         }
      }
   }
}