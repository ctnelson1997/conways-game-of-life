﻿//---------------------------------------------------------------------
// Group:   Pioneers
// Project: Conway's Game of Life
// Purpose: Extends from the abstract class. Is used when updating,
//          or advancing, a generation of cells. Keeps track of
//          the past and present arrays.
//---------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civilization.Models
{
   // -------------------------------------------------------------------
   // Extends from the abstract class. Is used when updating,
   // or advancing, a generation of cells. Keeps track of
   // the past and present arrays.
   // -------------------------------------------------------------------
   public class CommandUpdate : Command
   {

      private Cell.State[,] oldArr;
      private Cell.State[,] newArr;

      // -------------------------------------------------------------------
      // Constructor for the command, takes in the current array.
      // -------------------------------------------------------------------
      public CommandUpdate(Cell.State[,] oldArr)
      {
         this.oldArr = Util.CopyArray<Cell.State>(oldArr);
      }

      // -------------------------------------------------------------------
      // Executes the command -- updates the generation.
      // -------------------------------------------------------------------
      public override void Execute()
      {
         Register.instance.CommandUpdate();
      }

      // -------------------------------------------------------------------
      // Redoes the command, which is based on the newest generation
      // -------------------------------------------------------------------
      public override void Redo()
      {
         Cartographer.instance.Revert(newArr);
      }

      // -------------------------------------------------------------------
      // Undoes the command, which is based on the oldest generation
      // -------------------------------------------------------------------
      public override void Undo()
      {
         Cartographer.instance.Revert(oldArr);
      }

      // -------------------------------------------------------------------
      // Adds the new array, the next gen, to the command for revert
      // -------------------------------------------------------------------
      public void SetPostEffect(Cell.State[,] newArr)
      {
         this.newArr = Util.CopyArray<Cell.State>(newArr);
      }
   }
}
