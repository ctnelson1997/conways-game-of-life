﻿//---------------------------------------------------------------------
// Group:   Pioneers
// Project: Conway's Game of Life
// Purpose: The partial class for the main game view. Used with the
//          XAML.
//---------------------------------------------------------------------
using Civilization.Models;
using Civilization.SideMenu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Civilization
{
   // --------------------------------------------------------------------
   // Partial class to be paired with the xaml file for the main game
   // view. Primarily intializes commands and coordinates anmiations and 
   // events.
   // --------------------------------------------------------------------
   public partial class MainGameView : UserControl
   {
      #region INotifyPropertyChangedData
      public event PropertyChangedEventHandler PropertyChanged;

      protected void OnPropertyChanged(string propertyName)
      {
         var handler = PropertyChanged;

         if (handler != null)
         {
            handler(this, new PropertyChangedEventArgs(propertyName));
         }
      }

      private void NotifyPropertyChanged(string str)
      {
         if (PropertyChanged != null)
         {
            PropertyChanged(this, new PropertyChangedEventArgs(str));
         }
      }
      #endregion

      private static MainWindow instance;

      private Point? lastCenterPositionOnTarget;
      private Point? lastMousePositionOnTarget;
      private Point? lastDragPoint;
      private Tile lastDragTile;
      private Point rightClickedPoint;

      public static MainGameView thisMainGameView;

      public Register register { get; private set; }
      public Scheduler schedule { get; private set; }
      public Cartographer world { get; private set; }

      // -----------------------------------------------------------------
      // Contructor for the main game view class. Initializes components,
      // variables, and singleton instances.
      // -----------------------------------------------------------------
      public MainGameView()
      {
         thisMainGameView = this;

         world = Cartographer.instance;
         schedule = Scheduler.instance;
         register = Register.instance;

         InitializeComponent();

         InitializeDetailComponents();

         InitializeCommands();

      }

      // -----------------------------------------------------------------
      // Returns an instance of this class.
      // -----------------------------------------------------------------
      public static MainGameView GetThis()
      {
         return thisMainGameView;
      }

      // ----------------------------------------------------------------
      // Initalize all commands that my happen in the program.
      // ----------------------------------------------------------------
      public void InitializeCommands()
      {
         scrollViewer.ScrollChanged += OnScrollViewerScrollChanged;
         scrollViewer.MouseLeftButtonUp += OnMouseLeftButtonUp;
         scrollViewer.PreviewMouseLeftButtonUp += OnMouseLeftButtonUp;
         scrollViewer.PreviewMouseWheel += OnPreviewMouseWheel;
         scrollViewer.PreviewMouseLeftButtonDown += OnMouseLeftButtonDown;
         scrollViewer.PreviewMouseRightButtonDown += OnMouseRightButtonDown;
         scrollViewer.MouseMove += OnMouseMove;

         Loaded += MainWindow_Loaded;

         slider.ValueChanged += OnSliderValueChanged;
      }

      // ----------------------------------------------------------------
      // Initalize screen with the pallette class and remove visibility
      // for scroll bars.
      // ----------------------------------------------------------------
      public void InitializeDetailComponents()
      {
         scrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Hidden;
         scrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Hidden;

         grid.Background = Palette.DARK_BLACK;

         slider.Minimum = 1;
         slider.Visibility = Visibility.Collapsed;
      }

      // ----------------------------------------------------------------
      // When the right mouse button is pressed, the tile is mapped to 
      // the world.
      // ----------------------------------------------------------------
      public void OnMouseRightButtonDown(object sender, RoutedEventArgs e)
      {
         rightClickedPoint = Mouse.GetPosition(grid);
         lastDragTile = world.map[(int)rightClickedPoint.Y / world.tile_size]
            [(int)rightClickedPoint.X / world.tile_size];
      }

      // -----------------------------------------------------------------
      // Returns the user to the top left corner of the scrollviewer.
      // -----------------------------------------------------------------
      public void Home()
      {
         scrollViewer.ScrollToLeftEnd();
         scrollViewer.ScrollToTop();
      }

      // -----------------------------------------------------------------
      // Returns the user to the top left corner of the scrollviewer.
      // -----------------------------------------------------------------
      public static MainWindow GetInstance()
      {
         return instance;
      }

      // ----------------------------------------------------------------
      // When keys are pressed, the method calls the respective method 
      // for another class to perform an operation such as pausing the
      // game or clearing the world.
      // ----------------------------------------------------------------
      private void main_Window_KeyDown(object sender, KeyEventArgs e)
      {
         if (e.Key == Key.Space)
            Register.instance.Update();
         if (Keyboard.IsKeyDown(Key.Enter))
            schedule.ToggleState();
         if (Keyboard.IsKeyDown(Key.C))
         {
            world.Clear();
            schedule.ResetGenerations();
            schedule.Pause();
         }
         if (Keyboard.IsKeyDown(Key.H))
         {
            Home();
         }
         if (Keyboard.IsKeyDown(Key.O))
         {
            register.ToggleColorTracking();
         }
      }

      // ----------------------------------------------------------------
      // A method for checking once the window has been loaded. It will
      // use the actual size of the window to set the dimensions of the
      // grid.
      // ----------------------------------------------------------------
      public void MainWindow_Loaded(object sender, RoutedEventArgs e)
      {
         double window_Width;
         double window_Height;

         window_Width = mainGameView.ActualWidth;
         window_Height = mainGameView.ActualHeight;

         world.SetWindow(window_Width, window_Height);

         MainGrid.ItemsSource = world.map;

         //register.Seed();
      }

      // ----------------------------------------------------------------
      // A method for changing the current position of the display. It
      // will check to see if the last drag point has a value. If it does,
      // then htat means the left mouse button is being held down and the
      // user is attempting to pan the display. It will then get the
      // current position of the mouse and calculate how much the display
      // needs to pan.
      // ----------------------------------------------------------------
      public void OnMouseMove(object sender, MouseEventArgs e)
      {
         ViewOptionsMenuPanel VOMP = ViewOptionsMenuPanel.GetThis();
         Point p = e.GetPosition(grid);
         Tile currentDragTile;
         try
         {
            currentDragTile = world.map[(int)p.Y / world.tile_size][(int)p.X / world.tile_size];
         }
         catch (Exception err)
         {
            return; // OOB
         }
         if (currentDragTile != null && !currentDragTile.ToString().Equals(VOMP.MousePosition))
            VOMP.MousePosition = currentDragTile.ToString();

         if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
         {
            if (lastDragPoint.HasValue)
            {
               Point current_Position = e.GetPosition(scrollViewer);

               double change_In_X = current_Position.X - lastDragPoint.Value.X;
               double change_In_Y = current_Position.Y - lastDragPoint.Value.Y;

               lastDragPoint = current_Position;

               scrollViewer.ScrollToHorizontalOffset(scrollViewer.HorizontalOffset - change_In_X);
               scrollViewer.ScrollToVerticalOffset(scrollViewer.VerticalOffset - change_In_Y);
            }
         }
         else if (e.LeftButton == MouseButtonState.Pressed)
         {
            if (lastDragTile != null && !(lastDragTile.Equals(currentDragTile)))
            {
               if (Keyboard.IsKeyDown(Key.LeftShift))
               {
                  if (currentDragTile.cell.state != Cell.State.DEAD)
                  {
                     currentDragTile.cell.ChangeStateDead();
                  }
               }
               else if (register.gameMode == 1 && Keyboard.IsKeyDown(Key.LeftAlt))
               {
                  if (currentDragTile.cell.state != Cell.State.DEAD)
                  {
                     currentDragTile.cell.ChangeStateDead();
                  }
               }
               else if (currentDragTile.cell.state != Cell.State.ALIVE)
               {
                  currentDragTile.ChangeColor();
               }
            }
            lastDragTile = currentDragTile;
         }
      }

      // ----------------------------------------------------------------
      // A method for initiating the mouse drag to pan process. It will 
      // take the mouse position relative to the ScrollViewer and ensure
      // it is within the confines of the viewport (The Grid).
      // ----------------------------------------------------------------
      public void OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
      {
         int x;
         int y;

         Point p = Mouse.GetPosition(grid);

         x = (int)p.X / world.tile_size;

         y = (int)p.Y / world.tile_size;

         if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
         {
            Point current_Position = e.GetPosition(scrollViewer);

            if (current_Position.X <= scrollViewer.ViewportWidth && current_Position.Y < scrollViewer.ViewportHeight)
            {
               scrollViewer.Cursor = Cursors.Hand;
               lastDragPoint = current_Position;
               Mouse.Capture(scrollViewer);
            }
         }
         else if (Keyboard.IsKeyDown(Key.LeftAlt))
         {
            world.map[y][x].cell.ChangeStateDead();
            lastDragTile = world.map[y][x];
            schedule.Pause();
         }
         else
         {
            world.map[y][x].ChangeColor();
            lastDragTile = world.map[y][x];
            schedule.Pause();
         }
      }

      // ----------------------------------------------------------------
      // A method for releasing the mouse drag to pan process. It will return
      // the mouse icon to its standard icon and set the drag point to null.
      // ----------------------------------------------------------------
      public void OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
      {
         scrollViewer.Cursor = Cursors.Arrow;
         scrollViewer.ReleaseMouseCapture();
         lastDragPoint = null;
      }

      // ----------------------------------------------------------------
      // A method for changing the slider value as the mouse wheel moves. It
      // will check to see if the user is attempting to zoom in or out and
      // then will increment or decrement the value.
      // ----------------------------------------------------------------
      public void OnPreviewMouseWheel(object sender, MouseWheelEventArgs e)
      {
         if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
         {
            lastMousePositionOnTarget = Mouse.GetPosition(grid);

            if (e.Delta > 0)
            {
               slider.Value++;
            }

            if (e.Delta < 0)
            {
               slider.Value--;
            }

            e.Handled = true;
         }
      }

      // ----------------------------------------------------------------
      // Slider method for handling backround generation of the UI.
      // ----------------------------------------------------------------
      public void OnSliderValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
      {
         scaleTransform.ScaleX = e.NewValue;
         scaleTransform.ScaleY = e.NewValue;

         Point center_Of_Viewport = new Point(scrollViewer.ViewportWidth / 2, scrollViewer.ViewportHeight / 2);

         lastCenterPositionOnTarget = scrollViewer.TranslatePoint(center_Of_Viewport, grid);
      }

      // ----------------------------------------------------------------
      // Slider method for handling background generation of the UI.
      // ----------------------------------------------------------------
      public void OnScrollViewerScrollChanged(object sender, ScrollChangedEventArgs e)
      {
         if (e.ExtentHeightChange != 0 || e.ExtentWidthChange != 0)
         {
            Point? targetBefore = null;
            Point? targetNow = null;

            if (!lastMousePositionOnTarget.HasValue)
            {
               if (lastCenterPositionOnTarget.HasValue)
               {
                  var centerOfViewport = new Point(scrollViewer.ViewportWidth / 2, scrollViewer.ViewportHeight / 2);
                  Point centerOfTargetNow = scrollViewer.TranslatePoint(centerOfViewport, grid);

                  targetBefore = lastCenterPositionOnTarget;
                  targetNow = centerOfTargetNow;
               }
            }
            else
            {
               targetBefore = lastMousePositionOnTarget;
               targetNow = Mouse.GetPosition(grid);

               lastMousePositionOnTarget = null;
            }

            if (targetBefore.HasValue)
            {
               double dXInTargetPixels = targetNow.Value.X - targetBefore.Value.X;
               double dYInTargetPixels = targetNow.Value.Y - targetBefore.Value.Y;

               double multiplicatorX = e.ExtentWidth / (world.y_axis_length);
               double multiplicatorY = e.ExtentHeight / (world.x_axis_length);

               double newOffsetX = scrollViewer.HorizontalOffset - dXInTargetPixels * multiplicatorX;
               double newOffsetY = scrollViewer.VerticalOffset - dYInTargetPixels * multiplicatorY;

               if (double.IsNaN(newOffsetX) || double.IsNaN(newOffsetY))
               {
                  return;
               }

               scrollViewer.ScrollToHorizontalOffset(newOffsetX);
               scrollViewer.ScrollToVerticalOffset(newOffsetY);
            }
         }
      }

      // ----------------------------------------------------------------
      // Spawns a glider when the option of glider is clicked.
      // ----------------------------------------------------------------
      private void SpawnGlider_Click(object sender, RoutedEventArgs e)
      {
         if (register.gameMode == 1)
            return;
         int x = (int)rightClickedPoint.X / world.tile_size;
         int y = (int)rightClickedPoint.Y / world.tile_size;
         Organism.GLIDER.Spawn(x, y);
      }

      // ----------------------------------------------------------------
      // Spawns a beehive when the option of beehive is clicked.
      // ----------------------------------------------------------------
      private void SpawnBeeHive_Click(object sender, RoutedEventArgs e)
      {
         if (register.gameMode == 1)
            return;
         int x = (int)rightClickedPoint.X / world.tile_size;
         int y = (int)rightClickedPoint.Y / world.tile_size;
         Organism.BEEHIVE.Spawn(x, y);
      }

      // ----------------------------------------------------------------
      // Spawns a blinker when the option of blinker is clicked.
      // ----------------------------------------------------------------
      private void SpawnBlinker_Click(object sender, RoutedEventArgs e)
      {
         if (register.gameMode == 1)
            return;
         int x = (int)rightClickedPoint.X / world.tile_size;
         int y = (int)rightClickedPoint.Y / world.tile_size;
         Organism.BLINKER.Spawn(x, y);
      }
   }
}
