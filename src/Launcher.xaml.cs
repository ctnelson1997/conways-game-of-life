﻿//---------------------------------------------------------------------
// Group:   Pioneers
// Project: Conway's Game of Life
// Purpose: The partial class for the launcher. It is used with the
//          XAML.
//---------------------------------------------------------------------
using Civilization.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Civilization
{
   // --------------------------------------------------------------------
   // Partial class to be paired with the xaml file for the Launcher.
   // --------------------------------------------------------------------
   public partial class Launcher : UserControl
   {

      int world_axis_size;
      int total_world_size;
      int gamemode;

      private const double HELP_BOX_WIDTH = 250.0;

      Cartographer world;
      Register register;

      bool expanded;

      // -----------------------------------------------------------------
      // Contructor for the base game options menu panel class. 
      // Initializes components, variables, commands and singleton 
      // instances.
      // -----------------------------------------------------------------
      public Launcher()
      {
         InitializeComponent();

         expanded = false;

         XAMLWorldXSlider.ValueChanged += OnWorldXSliderChanged;
         XAMLGameModeComboBox.SelectionChanged += OnGameModeSelectionChanged;
         XAMLplayButton.Click += OnPlayButtonClick;
         XAMLHelpButton.Click += OnHelpButtonClick;

         world = Cartographer.instance;
         register = Register.instance;

         HelpPanel.Width = 0;

         world_axis_size = (int)XAMLWorldXSlider.Value;
         total_world_size = world_axis_size * world_axis_size;

         XAMLXTilesTextBox.Text = world_axis_size.ToString() + " x " 
            + world_axis_size.ToString() 
            + ", " 
            + total_world_size.ToString() 
            + " tiles.";

         XAMLGameModeComboBox.SelectedIndex = 0;
         gamemode = XAMLGameModeComboBox.SelectedIndex;
      }

      // -----------------------------------------------------------------
      // Command for the World Slider changed. It will change the size
      // of the world.
      // -----------------------------------------------------------------
      public void OnWorldXSliderChanged(object sender, RoutedEventArgs e)
      {
         world_axis_size = (int)XAMLWorldXSlider.Value;
         total_world_size = world_axis_size * world_axis_size;

         XAMLXTilesTextBox.Text = world_axis_size.ToString() + " x " 
            + world_axis_size.ToString() 
            + ", " 
            + total_world_size.ToString() + " tiles.";
      }

      // -----------------------------------------------------------------
      // Command for when the game mode combo box selection changed. It 
      // will change the gamemode.
      // -----------------------------------------------------------------
      public void OnGameModeSelectionChanged(object sender, RoutedEventArgs e)
      {
         gamemode = XAMLGameModeComboBox.SelectedIndex;
      }

      // -----------------------------------------------------------------
      // Command for when the play button is clicked. It will load up the
      // world.
      // -----------------------------------------------------------------
      public void OnPlayButtonClick(object sender, RoutedEventArgs e)
      {
         world.grid_size = world_axis_size;
         register.ChangeGameMode(gamemode);
      }

      // -----------------------------------------------------------------
      // Command for when the help button is clicked. It will show the 
      // help box.
      // -----------------------------------------------------------------
      public void OnHelpButtonClick(object sender, RoutedEventArgs e)
      {
         if (expanded)
         {
            HelpPanel.Width = 0;

            expanded = false;
         }
         else
         {
            HelpPanel.Width = HELP_BOX_WIDTH;

            expanded = true;
         }
      }
   }
}
