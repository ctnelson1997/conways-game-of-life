﻿//---------------------------------------------------------------------
// Group:   Pioneers
// Project: Conway's Game of Life
// Purpose: The partial class for the game options panel. Used with the
//          XAML.
//---------------------------------------------------------------------
using Civilization.Models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Civilization.SideMenu
{
   // --------------------------------------------------------------------
   // Partial class to be paired with the xaml file for the game options
   // menu panel. Primarily intializes commands and coordinates
   // anmiations and events.
   // --------------------------------------------------------------------
   public partial class GameOptionsMenuPanel : BaseMenuPanel
   {
      private int seed;
      private Register register;
      private Cartographer world;
      private Scheduler scheduler;

      // -----------------------------------------------------------------
      // Contructor for the base game options menu panel class. 
      // Initializes components, variables, and singleton instances.
      // -----------------------------------------------------------------
      public GameOptionsMenuPanel() : base()
      {
         InitializeComponent();

         InitializeVariables();

         InitializeBaseComponents();

         InitializeButtonList();

         InitializeCommands();

         register = Register.instance;
         world = Cartographer.instance;
         scheduler = Scheduler.instance;
      }

      // -----------------------------------------------------------------
      // Initializes all variables instantiated in the XAML code. Also, 
      // initializes the shrinkableControls list.
      // -----------------------------------------------------------------
      public override void InitializeVariables()
      {
         seed = 0;

         baseCanvas = XAMLbaseCanvas;
         displayCanvas = XAMLdisplayCanvas;
         bannerLabel = XAMLbannerLabel;

         pinButton = XAMLpinButton;
         minimizeButton = XAMLminimizeButton;
         displayStackPanel = XAMLDisplayStackPanel;

         seed = (int)XAMLSeedSlider.Value;
         XAMLSeedTextBox.Text = seed.ToString();
         XAMLLoadButton.Click += OnLoadButtonClick;
         XAMLSaveButton.Click += OnSaveButtonClick;
         XAMLClearButton.Click += OnClearButtonClick;

         KeyValuePair<Control, KeyValuePair<double, double>> pair;
         KeyValuePair<double, double> widthHeightPair;

         widthHeightPair = new KeyValuePair<double, double>(XAMLSeedButton.Width, XAMLSeedButton.Height);
         pair = new KeyValuePair<Control, KeyValuePair<double, double>>(XAMLSeedButton, widthHeightPair);
         shrinkableControls.Add(pair);

         widthHeightPair = new KeyValuePair<double, double>(XAMLSeedSlider.Width, XAMLSeedSlider.Height);
         pair = new KeyValuePair<Control, KeyValuePair<double, double>>(XAMLSeedSlider, widthHeightPair);
         shrinkableControls.Add(pair);

         widthHeightPair = new KeyValuePair<double, double>(XAMLLoadButton.Width, XAMLLoadButton.Height);
         pair = new KeyValuePair<Control, KeyValuePair<double, double>>(XAMLLoadButton, widthHeightPair);
         shrinkableControls.Add(pair);

         widthHeightPair = new KeyValuePair<double, double>(XAMLSaveButton.Width, XAMLSaveButton.Height);
         pair = new KeyValuePair<Control, KeyValuePair<double, double>>(XAMLSaveButton, widthHeightPair);
         shrinkableControls.Add(pair);

         widthHeightPair = new KeyValuePair<double, double>(XAMLClearButton.Width, XAMLClearButton.Height);
         pair = new KeyValuePair<Control, KeyValuePair<double, double>>(XAMLClearButton, widthHeightPair);
         shrinkableControls.Add(pair);
      }

      // -----------------------------------------------------------------
      // Initializes commands for controls not defined in the base class.
      // -----------------------------------------------------------------
      public void InitializeCommands()
      {
         XAMLSeedSlider.ValueChanged += OnSeedSliderChanged;
         XAMLSeedButton.Click += OnSeedButtonClicked;
      }

      // -----------------------------------------------------------------
      // The command for when the Seed Slider's value is changed. It
      // updates the seed textbox.
      // -----------------------------------------------------------------
      public void OnSeedSliderChanged(object sender, RoutedEventArgs e)
      {
         seed = (int)XAMLSeedSlider.Value;
         XAMLSeedTextBox.Text = seed.ToString();
      }

      // -----------------------------------------------------------------
      // The command for when the Seed Button is clicked. It will clear
      // the world, change the seed, and reset the generations.
      // -----------------------------------------------------------------
      public void OnSeedButtonClicked(object sender, RoutedEventArgs e)
      {
         world.Clear();
         register.Seed(seed);
         scheduler.ResetGenerations();
      }

      // -----------------------------------------------------------------
      // The command for when the Load Button is clicked. It will open a
      // dialog box for the user to select a file to load into the game.
      // It will then load that file into the game.
      // -----------------------------------------------------------------
      public void OnLoadButtonClick(object sender, RoutedEventArgs e)
      {
         string filePath;

         OpenFileDialog dialogBox = new OpenFileDialog();
         dialogBox.ShowDialog();

         filePath = dialogBox.FileName;

         WorldUtil.Load(filePath);

         XAMLFilePathTextBox.Text = filePath;
      }

      // -----------------------------------------------------------------
      // The command for when the Save Button is clicked. It will open a
      // a dialog box for the user to select a place to save the file.
      // -----------------------------------------------------------------
      public void OnSaveButtonClick(object sender, RoutedEventArgs e)
      {
         string filePath;

         SaveFileDialog dialogBox = new SaveFileDialog();
         dialogBox.DefaultExt = ".cgol";
         dialogBox.ShowDialog();

         filePath = dialogBox.FileName;

         WorldUtil.Save(filePath);

         XAMLFilePathTextBox.Text = filePath;
      }

      // -----------------------------------------------------------------
      // The command for thwne the Clear Button is clicked. It clears the
      // world.
      // -----------------------------------------------------------------
      public void OnClearButtonClick(object sender, RoutedEventArgs e)
      {
         world.Clear();
      }
   }
}
