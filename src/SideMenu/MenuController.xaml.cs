﻿//---------------------------------------------------------------------
// Group:   Pioneers
// Project: Conway's Game of Life
// Purpose: The partial class for the Menu Controller. It manages all
//          menu panels.
//---------------------------------------------------------------------
using Civilization.Models;
using Civilization.SideMenu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Civilization.SideMenu
{
   // --------------------------------------------------------------------
   // Partial class to be paired with the xaml file for the menu
   // controller. It manages the menu panels.
   // --------------------------------------------------------------------
   public partial class MenuController : UserControl
   {
      private List<BaseMenuPanel> panelList;
      public Cartographer cartographer { get; set; }
      public Register register { get; set; }
      public Scheduler scheduler { get; set; }

      // -----------------------------------------------------------------
      // Contructor for the the menu controller class. Initializes
      // commands, singleton instances, and variables.
      // -----------------------------------------------------------------
      public MenuController()
      {
         InitializeComponent();

         InitializeList();

         SideMenuGridStackPanel.MouseEnter += OnMouseEnter;
         SideMenuGridStackPanel.MouseLeave += OnMouseLeave;

         cartographer = Cartographer.instance;
         register = Register.instance;
         scheduler = Scheduler.instance;

      }

      // -----------------------------------------------------------------
      // Initializes the list of menu panels.
      // -----------------------------------------------------------------
      private void InitializeList()
      {
         panelList = new List<BaseMenuPanel>();

         panelList.Add(timeMenuPanel);
         panelList.Add(gameOptionsMenuPanel);
         panelList.Add(viewOptionsMenuPanel);
         panelList.Add(organismHubMenuPanel);
      }

      // -----------------------------------------------------------------
      // The command for when the mouse enters the menu. It will expand
      // the panels. 
      // -----------------------------------------------------------------
      private void OnMouseEnter(object sender, MouseEventArgs e)
      {
         bool canAnimate = CanAnimationAllPanels();
         bool areAllCollapsed = AreAllPanelsCollapsed();

         TimeSpan beginTime = TimeSpan.FromMilliseconds(0);

         DoubleAnimation widthAnimation;
         DoubleAnimation heightAnimation;

         if (canAnimate)
         {
            for (int i = 0; i < panelList.Count; i++)
            {
               if (panelList[i].CanCollapse())
               {
                  widthAnimation = panelList[i].GenerateExapndWidthAnimation();
                  heightAnimation = panelList[i].GenerateExapndHeightAnimation();

                  if (areAllCollapsed)
                  {
                     heightAnimation.Duration = new Duration(TimeSpan.FromMilliseconds(0));
                     heightAnimation.BeginTime = TimeSpan.FromMilliseconds(0);
                  }
                  else
                  {
                     heightAnimation.Duration = new Duration(TimeSpan.FromMilliseconds(100));
                     heightAnimation.BeginTime = beginTime;
                     beginTime = beginTime + TimeSpan.FromMilliseconds(100);
                  }

                  Storyboard storyboard = new Storyboard();

                  widthAnimation.BeginTime = beginTime;

                  beginTime = beginTime + TimeSpan.FromMilliseconds(100);

                  storyboard.Children.Add(widthAnimation);
                  storyboard.Children.Add(heightAnimation);

                  storyboard.Begin(panelList[i].baseCanvas);
               }
            }

            menuGrid.Width = 250;
         }
      }

      // -----------------------------------------------------------------
      // The command for when the mouse enters the menu. It will collapse
      // the panels. 
      // -----------------------------------------------------------------
      private void OnMouseLeave(object sender, MouseEventArgs e)
      {
         TimeSpan beginTime = TimeSpan.FromMilliseconds(0);

         DoubleAnimation widthAnimation;
         DoubleAnimation heightAnimation;

         for (int i = 0; i < panelList.Count; i++)
         {
            if (panelList[i].CanCollapse())
            {
               Storyboard storyboard = new Storyboard();

               widthAnimation = panelList[i].GenerateCollapseWidthAnimation();

               widthAnimation.BeginTime = beginTime;

               beginTime = beginTime + widthAnimation.Duration.TimeSpan;

               storyboard.Children.Add(widthAnimation);

               storyboard.Begin(panelList[i].baseCanvas);

            }
         }

         beginTime = TimeSpan.FromMilliseconds(0);

         for (int i = 0; i < panelList.Count; i++)
         {
            if (panelList[i].CanCollapse())
            {
               Storyboard storyboard = new Storyboard();

               heightAnimation = panelList[i].GenerateCollapseHeightAnimation();

               heightAnimation.BeginTime = beginTime;

               beginTime = beginTime + heightAnimation.Duration.TimeSpan;

               storyboard.Children.Add(heightAnimation);

               storyboard.Begin(panelList[i].baseCanvas);
            }
         }

         menuGrid.Width = 20;
      }

      // -----------------------------------------------------------------
      // The commands for when the user enters on of the keys. It will
      // intantiate some of the shortcuts. 
      // -----------------------------------------------------------------
      private void OnKeyDown(object sender, KeyEventArgs e)
      {
         if (e.Key == Key.Space)
            Register.instance.Update();
         if (Keyboard.IsKeyDown(Key.Enter))
            scheduler.ToggleState();
         if (Keyboard.IsKeyDown(Key.C))
         {
            cartographer.Clear();
            scheduler.ResetGenerations();
            scheduler.Pause();
         }
         if (Keyboard.IsKeyDown(Key.H))
         {
            MainGameView MGV = MainGameView.GetThis();
            MGV.Home();
         }
         if (Keyboard.IsKeyDown(Key.O))
         {
            register.ToggleColorTracking();
         }
      }

      // -----------------------------------------------------------------
      // Returns if all the panels can be animated.
      // -----------------------------------------------------------------
      private bool CanAnimationAllPanels()
      {
         bool canAnimate = true;

         for (int i = 0; i < panelList.Count; i++)
            if (panelList[i].IsMouseOver && !panelList[i].CanCollapse())
               canAnimate = false;

         return canAnimate;
      }

      // -----------------------------------------------------------------
      // Returns if all the panels can be collapsed.
      // -----------------------------------------------------------------
      private bool AreAllPanelsCollapsed()
      {
         bool allCollapsed = true;

         for (int i = 0; i < panelList.Count; i++)
            if (panelList[i].baseCanvas.ActualWidth != 0)
               allCollapsed = false;

         return allCollapsed;
      }
   }
}
