﻿//---------------------------------------------------------------------
// Group:   Pioneers
// Project: Conway's Game of Life
// Purpose: The partial class for the organism hub panel. Used with the
//          XAML.
//---------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Civilization.SideMenu
{
   // --------------------------------------------------------------------
   // Partial class to be paired with the xaml file for the organism hub
   // menu panel. Primarily intializes commands and coordinates
   // anmiations and events.
   // --------------------------------------------------------------------
   public partial class OrganismHubMenuPanel : BaseMenuPanel
   {
      // -----------------------------------------------------------------
      // Contructor for the base oganism hub menu panel class. 
      // Initializes components, variables, and singleton instances.
      // -----------------------------------------------------------------
      public OrganismHubMenuPanel()
      {
         InitializeComponent();

         InitializeVariables();

         InitializeBaseComponents();

         InitializeButtonList();

         InitializeCommands();
      }

      // -----------------------------------------------------------------
      // Initializes all variables instantiated in the XAML code. Also, 
      // initializes the shrinkableControls list.
      // -----------------------------------------------------------------
      public override void InitializeVariables()
      {
         baseCanvas = XAMLbaseCanvas;
         displayCanvas = XAMLdisplayCanvas;
         bannerLabel = XAMLbannerLabel;

         pinButton = XAMLpinButton;
         minimizeButton = XAMLminimizeButton;
         displayStackPanel = XAMLDisplayStackPanel;

         KeyValuePair<Control, KeyValuePair<double, double>> pair;
         KeyValuePair<double, double> widthHeightPair;

         widthHeightPair = new KeyValuePair<double, double>(XAMLTrackingButton.Width, XAMLTrackingButton.Height);
         pair = new KeyValuePair<Control, KeyValuePair<double, double>>(XAMLTrackingButton, widthHeightPair);
         shrinkableControls.Add(pair);

         widthHeightPair = new KeyValuePair<double, double>(XAMLQuitButton.Width, XAMLQuitButton.Height);
         pair = new KeyValuePair<Control, KeyValuePair<double, double>>(XAMLQuitButton, widthHeightPair);
         shrinkableControls.Add(pair);
      }

      // -----------------------------------------------------------------
      // Initializes commands for controls not defined in the base class.
      // -----------------------------------------------------------------
      public void InitializeCommands()
      {
         XAMLTrackingButton.Click += OnTrackingButtonClick;
         XAMLQuitButton.Click += OnQuitButtonClicked;
      }

      // -----------------------------------------------------------------
      // Command for when the tracking button is clicked. It toggles
      // tracking.
      // -----------------------------------------------------------------
      public void OnTrackingButtonClick(object sender, RoutedEventArgs e)
      {
         register.ToggleColorTracking();
      }

      // -----------------------------------------------------------------
      // Command for when the Quit Button is clicked. Returns user to the
      // launcher.
      // -----------------------------------------------------------------
      public void OnQuitButtonClicked(object sender, RoutedEventArgs e)
      {
         register.ToggleColorTracking();
      }

   }
}
