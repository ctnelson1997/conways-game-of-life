﻿//---------------------------------------------------------------------
// Group:   Pioneers
// Project: Conway's Game of Life
// Purpose: The partial class for the time menu panel. Used with the
//          XAML.
//---------------------------------------------------------------------
using Civilization.CommandPattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Civilization.SideMenu
{
   // --------------------------------------------------------------------
   // Partial class to be paired with the xaml file for the time
   // menu panel. Primarily intializes commands and coordinates
   // anmiations and events.
   // --------------------------------------------------------------------
   public partial class TimeMenuPanel : BaseMenuPanel
   {
      const int TICKS_PER_SECOND = 1000;

      // -----------------------------------------------------------------
      // Contructor for the time menu panel class. Initializes components, 
      // variables, and singleton instances.
      // -----------------------------------------------------------------
      public TimeMenuPanel() : base()
      {
         InitializeComponent();

         InitializeVariables();

         InitializeBaseComponents();

         InitializeButtonList();

         InitilizeCommands();

         int gens = (int)XAMLspeedSlider.Value;
         XAMLspeedTextBox.Text = gens.ToString() + " gen/s";
         gens = (int)((1.0 / gens) * TICKS_PER_SECOND);
         scheduler.ChangeSpeed(gens);
      }

      // -----------------------------------------------------------------
      // Initializes all variables instantiated in the XAML code. Also, 
      // initializes the shrinkableControls list.
      // -----------------------------------------------------------------
      public override void InitializeVariables()
      {
         baseCanvas = XAMLbaseCanvas;
         displayCanvas = XAMLdisplayCanvas;
         bannerLabel = XAMLbannerLabel;

         pinButton = XAMLpinButton;
         minimizeButton = XAMLminimizeButton;
         displayStackPanel = XAMLDisplayStackPanel;

         KeyValuePair<Control, KeyValuePair<double, double>> pair;
         KeyValuePair<double, double> widthHeightPair;

         widthHeightPair = new KeyValuePair<double, double>(XAMLplayButton.Width, XAMLplayButton.Height);
         pair = new KeyValuePair<Control, KeyValuePair<double, double>>(XAMLplayButton, widthHeightPair);
         shrinkableControls.Add(pair);

         widthHeightPair = new KeyValuePair<double, double>(XAMLpauseButton.Width, XAMLpauseButton.Height);
         pair = new KeyValuePair<Control, KeyValuePair<double, double>>(XAMLpauseButton, widthHeightPair);
         shrinkableControls.Add(pair);

         widthHeightPair = new KeyValuePair<double, double>(XAMLspeedSlider.Width, XAMLspeedSlider.Height);
         pair = new KeyValuePair<Control, KeyValuePair<double, double>>(XAMLspeedSlider, widthHeightPair);
         shrinkableControls.Add(pair);

         widthHeightPair = new KeyValuePair<double, double>(XAMLRedoButton.Width, XAMLRedoButton.Height);
         pair = new KeyValuePair<Control, KeyValuePair<double, double>>(XAMLRedoButton, widthHeightPair);
         shrinkableControls.Add(pair);

         widthHeightPair = new KeyValuePair<double, double>(XAMLUndoButton.Width, XAMLUndoButton.Height);
         pair = new KeyValuePair<Control, KeyValuePair<double, double>>(XAMLUndoButton, widthHeightPair);
         shrinkableControls.Add(pair);
      }

      // -----------------------------------------------------------------
      // Initializes commands for controls not defined in the base class.
      // -----------------------------------------------------------------
      public void InitilizeCommands()
      {
         XAMLplayButton.Click += OnStartButtonClick;
         XAMLpauseButton.Click += OnPauseButtonClick;
         XAMLRedoButton.Click += OnRedoButtonClick;
         XAMLUndoButton.Click += OnUndoButtonClick;
         XAMLspeedSlider.ValueChanged += OnSpeedSliderValueChanged;
      }

      // -----------------------------------------------------------------
      // Command for when the start button is clicked. It starts the game.
      // -----------------------------------------------------------------
      public void OnStartButtonClick(object sender, RoutedEventArgs e)
      {
         scheduler.Resume();
      }

      // -----------------------------------------------------------------
      // Command for when the pause button is clicked. It pauses the game.
      // -----------------------------------------------------------------
      public void OnPauseButtonClick(object sender, RoutedEventArgs e)
      {
         scheduler.Pause();
      }

      // -----------------------------------------------------------------
      // Command for when the redo button is clicked. It redos the
      // generation.
      // -----------------------------------------------------------------
      public void OnRedoButtonClick(object sender, RoutedEventArgs e)
      {
         if (scheduler.time >= 0)
         {
            scheduler.Pause();
            CommandInvoker.instance.Redo();
            scheduler.IncrementGenerations();
         }
      }

      // -----------------------------------------------------------------
      // Command for when the undo button is clicked. It undoes the
      // generation.
      // -----------------------------------------------------------------
      public void OnUndoButtonClick(object sender, RoutedEventArgs e)
      {
         if (scheduler.time > 0)
         {
            scheduler.Pause();
            CommandInvoker.instance.Undo();
            scheduler.DecrementGenerations();
         }
      }

      // -----------------------------------------------------------------
      // Command for when the speed slider value is changed. It chenges
      // the game speed.
      // -----------------------------------------------------------------
      public void OnSpeedSliderValueChanged(object sender, RoutedEventArgs e)
      {
         int gens = (int)XAMLspeedSlider.Value;
         XAMLspeedTextBox.Text = gens.ToString() + " gen/s";
         gens = (int)((1.0 / gens) * TICKS_PER_SECOND);
         scheduler.ChangeSpeed(gens);
      }
   }
}
