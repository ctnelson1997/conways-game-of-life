﻿//---------------------------------------------------------------------
// Group:   Pioneers
// Project: Conway's Game of Life
// Purpose: The partial class for the view options panel. Used with the
//          XAML.
//---------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Civilization.SideMenu
{
   // --------------------------------------------------------------------
   // Partial class to be paired with the xaml file for the view options
   // menu panel. Primarily intializes commands and coordinates
   // anmiations and events.
   // --------------------------------------------------------------------
   public partial class ViewOptionsMenuPanel : BaseMenuPanel
   {
      private string _MousePosition;
      public string MousePosition
      {
         get
         {
            return _MousePosition;
         }
         set
         {
            _MousePosition = value;
            NotifyPropertyChanged("MousePosition");
         }
      }
      public static ViewOptionsMenuPanel viewOptionsMenuPanel { get; set; }

      // -----------------------------------------------------------------
      // Contructor for the view options menu panel class. 
      // Initializes components, variables, and singleton instances.
      // -----------------------------------------------------------------
      public ViewOptionsMenuPanel()
      {
         viewOptionsMenuPanel = this;

         InitializeComponent();

         InitializeVariables();

         InitializeBaseComponents();

         InitializeButtonList();
      }

      // -----------------------------------------------------------------
      // Returns the panel intance.
      // -----------------------------------------------------------------
      public static ViewOptionsMenuPanel GetThis()
      {
         return viewOptionsMenuPanel;
      }

      // -----------------------------------------------------------------
      // Initializes all variables instantiated in the XAML code. Also, 
      // initializes the shrinkableControls list.
      // -----------------------------------------------------------------
      public override void InitializeVariables()
      {
         baseCanvas = XAMLbaseCanvas;
         displayCanvas = XAMLdisplayCanvas;
         bannerLabel = XAMLbannerLabel;

         pinButton = XAMLpinButton;
         minimizeButton = XAMLminimizeButton;
         displayStackPanel = XAMLDisplayStackPanel;

         XAMLHomeButton.Click += OnHomeButtonClick;

         KeyValuePair<Control, KeyValuePair<double, double>> pair;
         KeyValuePair<double, double> widthHeightPair;

         widthHeightPair = new KeyValuePair<double, double>(XAMLHomeButton.Width, XAMLHomeButton.Height);
         pair = new KeyValuePair<Control, KeyValuePair<double, double>>(XAMLHomeButton, widthHeightPair);

         shrinkableControls.Add(pair);
      }

      // -----------------------------------------------------------------
      // Command for when the Hoem Button is clicked. It will return the
      // user to the top right corner.
      // -----------------------------------------------------------------
      public void OnHomeButtonClick(object sender, RoutedEventArgs e)
      {
         MainGameView MGV = MainGameView.GetThis();

         MGV.Home();
      }
   }
}
