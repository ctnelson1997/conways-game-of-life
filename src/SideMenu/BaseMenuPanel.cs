﻿//---------------------------------------------------------------------
// Group:   Pioneers
// Project: Conway's Game of Life
// Purpose: Base class for the menu panels. Contains all superclass
//          methods for the menu.
//---------------------------------------------------------------------
using Civilization;
using Civilization.SideMenu;
using Civilization.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.ComponentModel;

namespace Civilization.SideMenu
{
   // -------------------------------------------------------------------
   // Abstract class to be paired with the xaml file for all menu panels.
   // Contains all view information and commands related to each menu
   // menu panel.
   // -------------------------------------------------------------------
   public abstract partial class BaseMenuPanel : UserControl, INotifyPropertyChanged
   {
      #region INotifyPropertyChangedData
      public event PropertyChangedEventHandler PropertyChanged;

      public static event PropertyChangedEventHandler PropertyChanged2;

      protected void OnPropertyChanged(string propertyName)
      {
         var handler = PropertyChanged2;

         if (handler != null)
         {
            handler(this, new PropertyChangedEventArgs(propertyName));
         }
      }

      public void NotifyPropertyChanged(string str)
      {
         if (PropertyChanged != null)
         {
            PropertyChanged(this, new PropertyChangedEventArgs(str));
         }
      }
      #endregion

      public double maxWidth;      // In pixels
      public double maxHeight;     // In pixels

      protected const int MAX_BUTTON_WIDTH = 30;         // In pixels
      protected const double ACC_RATIO = .75;            // Adds to one (1)
      protected const double DEC_RATIO = .25;            // Adds to one (1)
      protected const double ENTER_OPACITY = 1;
      protected const double LEAVE_OPACITY = .5;
      protected const double MARGIN_THICKNESS = 5.0;      // Thickness for the normal margin
      public static int MAX_ANIMATION_TIME = 150;        // In milliseconds

      public Cartographer cartographer { get; set; }
      public Register register { get; set; }
      public Scheduler scheduler { get; set; }

      public Canvas baseCanvas { get; set; }
      public Canvas displayCanvas { get; set; }
      public Button pinButton { get; set; }
      public Button minimizeButton { get; set; }
      public Button informationButton { get; set; }
      public StackPanel displayStackPanel { get; set; }
      public Label bannerLabel { get; set; }

      public List<Button> buttonList;

      private bool isMinimized;
      private bool isPinned;
      private bool isCollapsed;

      public List<KeyValuePair<Control, KeyValuePair<double, double>>> shrinkableControls;

      public Storyboard MinimizeShrinkablesStoryboard;
      public Storyboard MaximizeShrinkablesStoryboard;

      // -------------------------------------------------------------------
      // Contructor for the base menu panel class. Initializes variables.
      // -------------------------------------------------------------------
      public BaseMenuPanel()
      {
         InitializeInstances();

         InitializeButtonList();
         isMinimized = false;
         isPinned = false;
         isCollapsed = false;

         shrinkableControls = new List<KeyValuePair<Control, KeyValuePair<double, double>>>();
         MinimizeShrinkablesStoryboard = new Storyboard();
         MaximizeShrinkablesStoryboard = new Storyboard();
      }

      // -------------------------------------------------------------------
      // Initializes the intances of each singleton
      // -------------------------------------------------------------------
      protected void InitializeInstances()
      {
         cartographer = Cartographer.instance;
         register = Register.instance;
         scheduler = Scheduler.instance;
      }

      // -------------------------------------------------------------------
      // Initializes the commands associated with each pin and shirnkable.
      // -------------------------------------------------------------------
      protected void InitializeBaseComponents()
      {
         maxWidth = baseCanvas.Width;
         maxHeight = baseCanvas.Height;

         pinButton.Click += OnPinButtonClicked;
         minimizeButton.Click += OnMinimizeButtonClicked;
         MinimizeShrinkablesStoryboard.Completed += OnMinimizationCompleted;
         MaximizeShrinkablesStoryboard.Completed += OnMaximizationCompleted;

         baseCanvas.MouseEnter += OnMouseEnter;
         baseCanvas.MouseLeave += OnMouseLeave;
      }

      // -------------------------------------------------------------------
      // Initializes the button list.
      // -------------------------------------------------------------------
      protected void InitializeButtonList()
      {
         buttonList = new List<Button>();

         buttonList.Add(pinButton);
         buttonList.Add(minimizeButton);
      }

      // -------------------------------------------------------------------
      // Returns a generic double animation for width and height.
      // -------------------------------------------------------------------
      protected DoubleAnimation GetDoubleAnimation(FrameworkElement control, DependencyProperty propertyPath, double to)
      {
         DoubleAnimation animation = GenericDoubleAnimation();
         if (propertyPath == WidthProperty)
            animation.From = control.ActualWidth;
         if (propertyPath == HeightProperty)
            animation.From = control.ActualHeight;

         animation.To = to;

         Storyboard.SetTargetName(animation, control.Name);
         Storyboard.SetTargetProperty(animation, new PropertyPath(propertyPath));

         return animation;
      }

      // -------------------------------------------------------------------
      // Creates a storyboard for the animations related to the collapsable
      // buttons.
      // -------------------------------------------------------------------
      protected void AnimationButtons(double to)
      {
         Storyboard storyboard;

         DoubleAnimation widthAnimation;

         TimeSpan beginTime = TimeSpan.FromMilliseconds(0);

         for (int i = 0; i < buttonList.Count; i++)
         {
            storyboard = new Storyboard();

            widthAnimation = GetDoubleAnimation(buttonList[i], WidthProperty, to);

            widthAnimation.BeginTime = beginTime;

            beginTime = beginTime + widthAnimation.Duration.TimeSpan;

            storyboard.Children.Add(widthAnimation);

            storyboard.Begin(buttonList[i]);
         }
      }

      // -------------------------------------------------------------------
      // Gets a generic double using the To parameter.
      // -------------------------------------------------------------------
      protected void GenerateDoubleAnimationWithParameters(double to)
      {
         Storyboard storyboard;

         DoubleAnimation widthAnimation;

         TimeSpan beginTime = TimeSpan.FromMilliseconds(0);

         for (int i = 0; i < shrinkableControls.Count; i++)
         {
            storyboard = new Storyboard();

            widthAnimation = GetDoubleAnimation(buttonList[i], WidthProperty, to);

            widthAnimation.BeginTime = beginTime;

            beginTime = beginTime + widthAnimation.Duration.TimeSpan;

            storyboard.Children.Add(widthAnimation);

            storyboard.Begin(buttonList[i]);
         }
      }

      // -------------------------------------------------------------------
      // Gets a generic double animation that does not have any target or
      // property.
      // -------------------------------------------------------------------
      protected DoubleAnimation GenericDoubleAnimation()
      {
         DoubleAnimation animation = new DoubleAnimation();

         animation.Duration = new Duration(TimeSpan.FromMilliseconds(MAX_ANIMATION_TIME));
         animation.AutoReverse = false;
         animation.AccelerationRatio = ACC_RATIO;
         animation.DecelerationRatio = DEC_RATIO;

         return animation;
      }

      // -------------------------------------------------------------------
      // The command for when the pin button is clicked. It pins the panel
      // to the side of the screen.
      // -------------------------------------------------------------------
      public void OnPinButtonClicked(object sender, RoutedEventArgs e)
      {
         if (isPinned == false)
         {
            isPinned = true;
            pinButton.Background = new BrushConverter().ConvertFromString("#FF007ACC") as SolidColorBrush;
            bannerLabel.Background = new BrushConverter().ConvertFromString("#FF007ACC") as SolidColorBrush;
         }
         else
         {
            isPinned = false;
            pinButton.Background = new BrushConverter().ConvertFromString("#FF2D2D30") as SolidColorBrush;
            bannerLabel.Background = new BrushConverter().ConvertFromString("#FF2D2D30") as SolidColorBrush;
         }
      }

      // -------------------------------------------------------------------
      // The command for when the mouse enters the panel. It will expand the
      // buttons and highlight the panel.
      // -------------------------------------------------------------------
      public void OnMouseEnter(object sender, MouseEventArgs e)
      {
         Storyboard storyboard = new Storyboard();

         DoubleAnimation widthAnimation;
         DoubleAnimation opacityAnimation;

         if (isPinned && isMinimized)
         {
            MaximizeShrinkables();
         }
         else if (baseCanvas.ActualWidth == maxWidth)
         {
            widthAnimation = GetDoubleAnimation(bannerLabel, WidthProperty, maxWidth + MAX_BUTTON_WIDTH);
            opacityAnimation = GetDoubleAnimation(displayStackPanel, OpacityProperty, ENTER_OPACITY);

            storyboard.Children.Add(widthAnimation);
            storyboard.Children.Add(opacityAnimation);

            storyboard.Begin(baseCanvas);

            AnimationButtons(MAX_BUTTON_WIDTH);
         }
      }

      // -------------------------------------------------------------------
      // The command for when the mouse leaves the panel. It will collapse
      // the panel.
      // -------------------------------------------------------------------
      public void OnMouseLeave(object sender, MouseEventArgs e)
      {
         Storyboard storyboard = new Storyboard();

         DoubleAnimation widthAnimation;
         DoubleAnimation opacityAnimation;

         if (!isPinned)
         {
            widthAnimation = GetDoubleAnimation(bannerLabel, WidthProperty, maxWidth);
            opacityAnimation = GetDoubleAnimation(displayStackPanel, OpacityProperty, LEAVE_OPACITY);

            storyboard.Children.Add(widthAnimation);
            storyboard.Children.Add(opacityAnimation);

            storyboard.Begin(baseCanvas);

            AnimationButtons(0);
         }
         else
         {
            Minimizeshrinkables();
         }

      }

      // -------------------------------------------------------------------
      // Returns the canCollapse variable.
      // -------------------------------------------------------------------
      public bool CanCollapse()
      {
         return !isPinned;
      }

      // -------------------------------------------------------------------
      // Returns the isPinned variable.
      // -------------------------------------------------------------------
      public bool GetIsPinned()
      {
         return isPinned;
      }

      // -------------------------------------------------------------------
      // Returns the animation to expand the panel to its maximum width.
      // -------------------------------------------------------------------
      public DoubleAnimation GenerateExapndWidthAnimation()
      {
         DoubleAnimation animation = GenericDoubleAnimation();

         animation.From = baseCanvas.ActualWidth;
         animation.To = maxWidth;

         Storyboard.SetTargetName(animation, baseCanvas.Name);
         Storyboard.SetTargetProperty(animation, new PropertyPath(ListBoxItem.WidthProperty));

         return animation;
      }

      // -------------------------------------------------------------------
      // Returns the animation to expand the panel to its maximum height.
      // -------------------------------------------------------------------
      public DoubleAnimation GenerateExapndHeightAnimation()
      {
         DoubleAnimation animation = GenericDoubleAnimation();

         animation.From = baseCanvas.ActualHeight;
         animation.To = maxHeight;

         Storyboard.SetTargetName(animation, baseCanvas.Name);
         Storyboard.SetTargetProperty(animation, new PropertyPath(ListBoxItem.HeightProperty));

         return animation;
      }

      // -------------------------------------------------------------------
      // Returns the animation to collapse the panel to a width of 0.
      // -------------------------------------------------------------------
      public DoubleAnimation GenerateCollapseWidthAnimation()
      {
         DoubleAnimation animation = GenericDoubleAnimation();

         animation.From = baseCanvas.ActualWidth;
         animation.To = 0;

         Storyboard.SetTargetName(animation, baseCanvas.Name);
         Storyboard.SetTargetProperty(animation, new PropertyPath(ListBoxItem.WidthProperty));

         return animation;
      }

      // -------------------------------------------------------------------
      // Returns the animation to collapse the panel to a height of 0.
      // -------------------------------------------------------------------
      public DoubleAnimation GenerateCollapseHeightAnimation()
      {
         DoubleAnimation animation = GenericDoubleAnimation();

         animation.From = baseCanvas.ActualHeight;
         animation.To = 0;

         Storyboard.SetTargetName(animation, baseCanvas.Name);
         Storyboard.SetTargetProperty(animation, new PropertyPath(ListBoxItem.HeightProperty));

         return animation;
      }

      // -------------------------------------------------------------------
      // Returns the animation to minimize the panel to minWidth.
      // -------------------------------------------------------------------
      public DoubleAnimation GenerateMinimizeWidthAnimation()
      {
         double toWidtht;

         toWidtht = displayStackPanel.Margin.Left;
         toWidtht += displayStackPanel.ActualWidth;
         toWidtht += displayStackPanel.Margin.Right;
         DoubleAnimation animation = GenericDoubleAnimation();

         animation.From = baseCanvas.ActualWidth;
         animation.To = toWidtht;

         Storyboard.SetTargetName(animation, baseCanvas.Name);
         Storyboard.SetTargetProperty(animation, new PropertyPath(WidthProperty));

         return animation;
      }

      // -------------------------------------------------------------------
      // Returns the animation to minimize the panel to minHeight.
      // -------------------------------------------------------------------
      public DoubleAnimation GenerateMinimizeHeightAnimation()
      {
         double toHeight;

         toHeight = displayStackPanel.Margin.Top;
         toHeight += displayStackPanel.ActualHeight;
         toHeight += displayStackPanel.Margin.Bottom;
         DoubleAnimation animation = GenericDoubleAnimation();

         animation.From = baseCanvas.ActualHeight;
         animation.To = toHeight;

         Storyboard.SetTargetName(animation, baseCanvas.Name);
         Storyboard.SetTargetProperty(animation, new PropertyPath(HeightProperty));

         return animation;
      }

      // -------------------------------------------------------------------
      // An abstract method to initialize the buttons and vairables.
      // -------------------------------------------------------------------
      public abstract void InitializeVariables();

      // -------------------------------------------------------------------
      // The command for when the minimize button is clicked. It minimizes
      // the panel.
      // -------------------------------------------------------------------
      public void OnMinimizeButtonClicked(object sender, RoutedEventArgs e)
      {
         Minimizeshrinkables();
      }

      // -------------------------------------------------------------------
      // The command for when the maximize button is clicked. It maximizes
      // the panel.
      // -------------------------------------------------------------------
      public void OnMaximizeButtonClicked(object sender, RoutedEventArgs e)
      {
         MaximizeShrinkables();
      }

      // -------------------------------------------------------------------
      // Iterates through the list of shrinkables and shrinks them all to 0 
      // width and 0 height.
      // -------------------------------------------------------------------
      public void Minimizeshrinkables()
      {
         DoubleAnimation animation;

         for (int i = 0; i < shrinkableControls.Count; i++)
         {
            animation = GetDoubleAnimation(shrinkableControls[i].Key, WidthProperty, 0);
            MinimizeShrinkablesStoryboard.Children.Add(animation);
            animation = GetDoubleAnimation(shrinkableControls[i].Key, HeightProperty, 0);
            MinimizeShrinkablesStoryboard.Children.Add(animation);

            MinimizeShrinkablesStoryboard.Begin(shrinkableControls[i].Key);
         }

         isMinimized = true;
      }

      // -------------------------------------------------------------------
      // The command for when the minimization is completed. It will set all
      // the margins to zero.
      // -------------------------------------------------------------------
      public void OnMinimizationCompleted(object sender, EventArgs e)
      {
         Storyboard storyboard = new Storyboard();
         DoubleAnimation animation;

         for (int i = 0; i < shrinkableControls.Count; i++)
         {
            shrinkableControls[i].Key.Margin = new Thickness(0.0, 0.0, 0.0, 0.0);
         }

         animation = GenerateMinimizeWidthAnimation();
         storyboard.Children.Add(animation);
         animation = GenerateMinimizeHeightAnimation();
         storyboard.Children.Add(animation);

         storyboard.Begin(baseCanvas);

         minimizeButton.Background = new BrushConverter().ConvertFromString("#FF8631C7") as SolidColorBrush;
      }

      // -------------------------------------------------------------------
      // Iterates through the list of shrinkables and maximizes them all to 
      // thier maximum width and height
      // -------------------------------------------------------------------
      public void MaximizeShrinkables()
      {
         DoubleAnimation animation;

         animation = GenerateExapndHeightAnimation();
         MaximizeShrinkablesStoryboard.Children.Add(animation);
         animation = GenerateExapndWidthAnimation();
         MaximizeShrinkablesStoryboard.Children.Add(animation);

         MaximizeShrinkablesStoryboard.Begin(baseCanvas);

         isMinimized = false;
         minimizeButton.Background = new BrushConverter().ConvertFromString("#FF2D2D30") as SolidColorBrush;
      }

      // -------------------------------------------------------------------
      // The command for when the maximization is completed. It will set all
      // the margins to the shrinkable control's maxHeight and maxWidth.
      // -------------------------------------------------------------------
      public void OnMaximizationCompleted(object sender, EventArgs e)
      {
         Control c;
         double width;
         double height;

         Storyboard storyboard = new Storyboard();
         DoubleAnimation animation;

         for (int i = 0; i < shrinkableControls.Count; i++)
         {
            c = shrinkableControls[i].Key;
            width = shrinkableControls[i].Value.Key;
            height = shrinkableControls[i].Value.Value;

            animation = GetDoubleAnimation(c, WidthProperty, width);
            storyboard.Children.Add(animation);

            animation = GetDoubleAnimation(c, HeightProperty, height);
            storyboard.Children.Add(animation);

            shrinkableControls[i].Key.Margin = new Thickness(0.0, 0.0, MARGIN_THICKNESS, MARGIN_THICKNESS);

            storyboard.Begin(c);
         }
      }
   }
}
