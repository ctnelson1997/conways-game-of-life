﻿//---------------------------------------------------------------------
// Group:   Pioneers
// Project: Conway's Game of Life
// Purpose: Keeps track of the tile position and adjacencies. Also 
//          contains methods for updating tile properties.
//---------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;


namespace Civilization.Models
{
   // -------------------------------------------------------------------
   // Contains the information for where a cell is on the grid and the
   //    adjacent ones around it.
   // -------------------------------------------------------------------
   public class Tile : INotifyPropertyChanged
   {
      #region INotifyPropertyChangedData
      public event PropertyChangedEventHandler PropertyChanged;

      protected void OnPropertyChanged(string propertyName)
      {
         var handler = PropertyChanged;

         if (handler != null)
         {
            handler(this, new PropertyChangedEventArgs(propertyName));
         }
      }

      private void NotifyPropertyChanged(string str)
      {
         if (PropertyChanged != null)
         {
            PropertyChanged(this, new PropertyChangedEventArgs(str));
         }
      }
      #endregion
      
      public Point coordinate { get; private set; }


      private Cell _cell;
      public Cell cell
      {
         get
         {
            return _cell;
         }
         set
         {
            _cell = value;
            _cell.tile = this;
         }
      }

      // ----------------------------------------------------------------
      // Gets the adjacent tiles and stores.
      // ----------------------------------------------------------------
      public List<Tile> adjacencies { get; set; }

      // ----------------------------------------------------------------
      // Creates a DEAD tile at specified position. 
      // ----------------------------------------------------------------
      public Tile (int x, int y)
      {
         this.cell = new Cell(Cell.State.DEAD);
         coordinate = new Point(x, y);
      }

      // ----------------------------------------------------------------
      // Returns tile croodinates in string.
      // ----------------------------------------------------------------
      public override string ToString()
      {
         return "(" + coordinate.X.ToString() + ", " + coordinate.Y.ToString() + ")";
      }


      // ----------------------------------------------------------------
      // Changes the states of the cell (or "color")
      // ----------------------------------------------------------------
      public void ChangeColor()
      {
         this.cell.ChangeStatus();
         NotifyPropertyChanged("cell");
      }

      // ----------------------------------------------------------------
      // Updates the cell in the tile.
      // ----------------------------------------------------------------
      public void ForceUpdate()
      {
         NotifyPropertyChanged("cell");
      }

      // ----------------------------------------------------------------
      // Contains the Main Algorithms for Conway's Game of Life,
      //    Wire World, and High Life.
      // ----------------------------------------------------------------
      public void UpdateStatus()
      {
         switch (Register.instance.gameMode)
         {
            case 0: //Conways Game Of Life
               int aliveCG = 0;
               foreach (Tile tile in adjacencies)
                  if (tile != null && tile.cell.state == Cell.State.ALIVE)
                     aliveCG++;
               if ((aliveCG == 2 || aliveCG == 3) && this.cell.state == Cell.State.ALIVE)
                  this.cell.IncrementLifetime();
               else if (aliveCG == 3 && this.cell.state == Cell.State.DEAD)
                  this.cell.changeNext = true;
               else if (this.cell.state != Cell.State.DEAD)
                  this.cell.changeNext = true;
               break;

            case 1: //Wire World
               int electHead = 0;
               foreach (Tile tile in adjacencies)
                  if (tile != null && tile.cell.state == Cell.State.ELECTRONHEAD)
                     electHead++;
               if ((electHead == 1 || electHead == 2) && this.cell.state == Cell.State.CONDUCTOR)
                  this.cell.changeNext = true;
               else if (this.cell.state == Cell.State.ELECTRONHEAD)
                  this.cell.changeNext = true;
               else if (this.cell.state == Cell.State.ELECTRONTAIL)
                  this.cell.changeNext = true;
               break;

            case 2: //High Life
               int aliveHL = 0;
               foreach (Tile tile in adjacencies)
                  if (tile != null && tile.cell.state == Cell.State.ALIVE)
                     aliveHL++;
               if ((aliveHL == 2 || aliveHL == 3) && this.cell.state == Cell.State.ALIVE)
                  this.cell.IncrementLifetime();
               else if ((aliveHL == 3 || aliveHL == 6) && this.cell.state == Cell.State.DEAD)
                  this.cell.changeNext = true;
               else if (this.cell.state != Cell.State.DEAD)
                  this.cell.changeNext = true;
               break;

            case 3: //Conways Game Of Life with Coloring
               int aliveCGW = 0;
               foreach (Tile tile in adjacencies)
                  if (tile != null && tile.cell.state == Cell.State.ALIVE)
                     aliveCGW++;
               if ((aliveCGW == 2 || aliveCGW == 3) && this.cell.state == Cell.State.ALIVE)
                  this.cell.IncrementLifetime();
               else if (aliveCGW == 3 && (this.cell.state == Cell.State.DEAD || this.cell.state == Cell.State.WALL))
                  this.cell.changeNext = true;
               else if (this.cell.state != Cell.State.DEAD && this.cell.state != Cell.State.WALL)
                  this.cell.changeNext = true;
               break;
         }
      }

      // ----------------------------------------------------------------
      // Checks whether the tile's cell's state is changing and excecutes
      // ----------------------------------------------------------------
      public void Finish()
      {
         if(this.cell.changeNext)
         {
            this.cell.changeNext = false;
            this.cell.ChangeStatus();
            NotifyPropertyChanged("cell");
         }
         
      }
   }

}
