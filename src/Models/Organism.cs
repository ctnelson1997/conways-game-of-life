﻿//---------------------------------------------------------------------
// Group:   Pioneers
// Project: Conway's Game of Life
// Purpose: A representation of an organism in Conway's GoL.
//          These organisms can be found either by inspection or
//          the wikipedia page. Organisms are a special collection
//          of cells.
//---------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Civilization.Models
{
   // -------------------------------------------------------------------
   // A representation of an organism in Conway's GoL.
   // These organisms can be found either by inspection or
   // the wikipedia page. Organisms are a special collection
   // of cells.
   // -------------------------------------------------------------------
   public class Organism
   {
      public static readonly Organism GLIDER = new Organism("Glider", Form.GLIDER, Brushes.Aqua);
      public static readonly Organism BEEHIVE = new Organism("Beehive", Form.BEEHIVE, Brushes.Goldenrod);
      public static readonly Organism BLINKER = new Organism("Blinker", Form.BLINKER, Brushes.IndianRed);
      public static List<Organism> ORGANISMS { get; private set; }

      public string name { get; private set; }
      private Form form;
      private Brush color;

      // ----------------------------------------------------------------
      // Constructor with specific pattern and name
      // ----------------------------------------------------------------
      private Organism(string name, Form form)
      {
         if(ORGANISMS == null)
            ORGANISMS = new List<Organism>();
         this.name = name;
         this.form = form;
         ORGANISMS.Add(this);
      }

      // ----------------------------------------------------------------
      // Constructor with specific pattern, name, and color
      // ----------------------------------------------------------------
      private Organism(string name, Form form, Brush color) : this(name, form)
      {
         this.color = color;
      }

      //-----------------------------------------------------------------
      // Identifies all possible forms, patterns, and rotations
      // of an organism on the map and colors them.
      //-----------------------------------------------------------------
      public int Identify()
      {
         int numFound = 0;
         foreach(Form.Pattern pattern in form.patterns)
         {
            int point = 0;
            bool[,] boolMap = Cartographer.instance.boolMap;
            while (point < boolMap.Length)
            {
               Tuple<int, bool[,]> pointInfo = Util.FindPoint<bool>(point, boolMap, pattern.boolMap);
               point = pointInfo.Item1;
               bool[,] foundPattern = pointInfo.Item2;
               if (point == -1)
                  break;
               if (Util.IsValidPadding(point, boolMap, foundPattern, false))
               {
                  Color(boolMap, pointInfo, color);
                  numFound++;
               }
               point++;
            }
         }
         return numFound;
      }

      //-----------------------------------------------------------------
      // Colors an organism on the screen
      //-----------------------------------------------------------------
      private void Color(bool[,] data, Tuple<int, bool[,]> pair, Brush color)
      {
         int point = pair.Item1;
         bool[,] find = pair.Item2;
         int findLen = find.Length;
         int dataX = point % data.GetLength(0);
         int dataY = point / data.GetLength(0);
         for (int j = 0; j < findLen; j++)
         {
            int findX = j % find.GetLength(1);
            int findY = j / find.GetLength(1);
            if (find[findY, findX] == true)
               ColorTracker.instance.Add(Cartographer.instance.map[Util.Mod(dataY + findY, data.GetLength(0))][Util.Mod(dataX + findX, data.GetLength(1))].cell, color);
         }
      }

      // ----------------------------------------------------------------
      // Spawns the pattern on the map
      // ----------------------------------------------------------------
      public void Spawn(int x, int y)
      {
         Cell.State[,] grid = form.patterns[0].grid;
         int lengthI = grid.GetLength(0);
         int lengthJ = grid.GetLength(1);
         for (int i = 0; i < lengthI; i++)
            for (int j = 0; j < lengthJ; j++)
               Cartographer.instance.map[y + i][x + j].cell.state = (grid[i, j]);
      }

      //-----------------------------------------------------------------
      // Responsible for all the possible patterns of an organism, for
      // example a glider moves in 4 different positions (patterns)
      // and we must know each one of them.
      //-----------------------------------------------------------------
      private class Form
      {
         public static readonly Form GLIDER = new Form(new Pattern[4]
         { Pattern.GLIDER_0, Pattern.GLIDER_1, Pattern.GLIDER_2, Pattern.GLIDER_3 });

         public static readonly Form BEEHIVE = new Form(new Pattern[1]
         { Pattern.BEEHIVE_0 });

         public static readonly Form BLINKER = new Form(new Pattern[1]
         { Pattern.BLINKER_0 });

         public Pattern[] patterns { get; private set; }

         //--------------------------------------------------------------
         // Constructs a form with the given patterns
         //--------------------------------------------------------------
         public Form(Pattern[] patterns)
         {
            this.patterns = patterns;
         }

         // -------------------------------------------------------------
         // A 2d array representing the cell states of an organism.
         // Made seperate and modular for replacing with dynamic
         // file reading later on.
         // -------------------------------------------------------------
         public class Pattern
         {
            public static readonly Pattern GLIDER_0 = new Pattern(new Cell.State[3, 3]
            { {Cell.State.DEAD, Cell.State.ALIVE, Cell.State.DEAD },
              {Cell.State.DEAD, Cell.State.DEAD, Cell.State.ALIVE},
              {Cell.State.ALIVE, Cell.State.ALIVE, Cell.State.ALIVE} });

            public static readonly Pattern GLIDER_1 = new Pattern(new Cell.State[3, 3]
            { {Cell.State.ALIVE, Cell.State.DEAD, Cell.State.ALIVE },
              {Cell.State.DEAD, Cell.State.ALIVE, Cell.State.ALIVE},
              {Cell.State.DEAD, Cell.State.ALIVE, Cell.State.DEAD} });

            public static readonly Pattern GLIDER_2 = new Pattern(new Cell.State[3, 3]
            { {Cell.State.DEAD, Cell.State.DEAD, Cell.State.ALIVE },
              {Cell.State.ALIVE, Cell.State.DEAD, Cell.State.ALIVE},
              {Cell.State.DEAD, Cell.State.ALIVE, Cell.State.ALIVE} });

            public static readonly Pattern GLIDER_3 = new Pattern(new Cell.State[3, 3]
            { {Cell.State.ALIVE, Cell.State.DEAD, Cell.State.DEAD },
              {Cell.State.DEAD, Cell.State.ALIVE, Cell.State.ALIVE},
              {Cell.State.ALIVE, Cell.State.ALIVE, Cell.State.DEAD} });

            public static readonly Pattern BEEHIVE_0 = new Pattern(new Cell.State[3, 4]
            { {Cell.State.DEAD, Cell.State.ALIVE, Cell.State.ALIVE, Cell.State.DEAD },
              {Cell.State.ALIVE, Cell.State.DEAD, Cell.State.DEAD, Cell.State.ALIVE},
              {Cell.State.DEAD, Cell.State.ALIVE, Cell.State.ALIVE, Cell.State.DEAD } });

            public static readonly Pattern BLINKER_0 = new Pattern(new Cell.State[1, 3]
            { {Cell.State.ALIVE, Cell.State.ALIVE, Cell.State.ALIVE } });

            public Cell.State[,] grid { get; private set; }
            public bool[,] boolMap { get; private set; }

            // ----------------------------------------------------------
            // Saves the pattern to the object
            // ----------------------------------------------------------
            private Pattern(Cell.State[,] grid)
            {
               this.grid = grid;
               this.GenerateBoolMap();
            }

            // ----------------------------------------------------------
            // Generates a boolean map for quicker calculations
            // ----------------------------------------------------------
            private void GenerateBoolMap()
            {
               int xLength = grid.GetLength(0);
               int yLength = grid.GetLength(1);
               boolMap = new bool[xLength, yLength];
               for (int i = 0; i < xLength; i++)
                  for (int j = 0; j < yLength; j++)
                     boolMap[i, j] = grid[i, j].Equals(Cell.State.ALIVE) ? true : false;
            }
         }
      }
   }
}
