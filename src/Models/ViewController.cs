﻿//---------------------------------------------------------------------
// Group:   Pioneers
// Project: Conway's Game of Life
// Purpose: Handle communication between GUI and policy
//---------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civilization.Models
{
   //------------------------------------------------------------------
   // Theoritically controls communication between
   // the GUI and policy layer. Will be implemented fully
   // in a later version.
   //------------------------------------------------------------------
   public class ViewController : INotifyPropertyChanged
    {
        #region Singleton Properties
        private static ViewController _instance = new ViewController();
        public static ViewController instance
        {
            get
            {
                return _instance;
            }
        }

        private ViewController()
        {
            mousePoint = DEFAULT_MOUSE_POINT;
            world = Cartographer.instance;
            register = Register.instance;
            scheduler = Scheduler.instance;
        }
        #endregion

        #region INotifyPropertyChangedData
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void NotifyPropertyChanged(string str)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(str));
            }
        }
        #endregion

        public Cartographer world { get; set; }
        public Register register { get; set; }
        public Scheduler scheduler { get; private set; }

        private const string DEFAULT_MOUSE_POINT = "(0,0)";

      //------------------------------------------------------------------
      // Responsible for tracking the mouse position for coordination
      // with the side panels
      //------------------------------------------------------------------
      private string _mousePoint;
        public string mousePoint
        {
            get
            {
                return _mousePoint;
            }
            set
            {
                _mousePoint = value;
                NotifyPropertyChanged("mousePoint");
            }
        }
    }
}
