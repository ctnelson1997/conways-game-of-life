﻿//---------------------------------------------------------------------
// Group:   Pioneers
// Project: Conway's Game of Life
// Purpose: Manages things to do with the side menu.
//---------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Civilization.Models
{
    // -------------------------------------------------------------------
    // The class that manages things to do with the side menu.
    // -------------------------------------------------------------------
    public class SideMenuPanel
    {
        public double backgroundCanvasHeight;
        public Canvas backgroundCanvas { get; set; }

        public Canvas foregroundCanvas { get; set; }

        public Label headerLabel { get; set; }

        public bool canCollapse { get; set; }

        public Button pinButton { get; set; }

        // -------------------------------------------------------------------
        // Basic constructor to build and assign data.
        // -------------------------------------------------------------------
        public SideMenuPanel(Canvas backgroundCanvas, Canvas foregroundCanvas, bool canCollapse, Button pinButton, Label labelHeader)
        {
            this.backgroundCanvas = backgroundCanvas;
            this.foregroundCanvas = foregroundCanvas;
            this.canCollapse = canCollapse;
            this.pinButton = pinButton;
            this.headerLabel = labelHeader;

            backgroundCanvasHeight = backgroundCanvas.Height;
        }
    }
}
