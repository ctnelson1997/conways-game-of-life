﻿//---------------------------------------------------------------------
// Group:   Pioneers
// Project: Conway's Game of Life
// Purpose: Misc. utilities mostly for tracking
//---------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Civilization.Models
{
   // --------------------------------------------------------------
   //         _______________________ _
   //       ,'     _j'_.'         .'``.\
   //      /      /,--.\         /,--. \\
   //     |      |({{}})|       |/,--,\||
   //     |       \`--'/        ||\___///
   //      \       /""/\         \`.__//
   //       `.____/  /._`.________`._.'   
   //            /  /
   //           /,'\|
   //
   // My diploma for making this.
   // Responsible for all the mathematics behind submatrix
   // calculations, including searching, identifying, coloring,
   // padding, and rotations.
   // --------------------------------------------------------------
   public class Util
   {
      //------------------------------------------------------------------
      // Adapted from answer on
      // https://stackoverflow.com/questions/42250539/c-sharp-find-2d-small-array-in-2d-big-array
      // Factored out the matching algorithim as it is used elsewhere.
      // Also now iterates over multiple rotations, and returns a tuple
      // of the position and found rotation of find.
      //------------------------------------------------------------------
      public static Tuple<int, T[,]> FindPoint<T>(int startPoint, T[,] data, T[,] find)
      {
         T[][,] rotations = GenerateRotations<T>(find);
         int dataLen = data.Length;
         int findLen = find.Length;

         for (int i = startPoint; i < dataLen; i++)
         {
            int dataX = i % data.GetLength(0);
            int dataY = i / data.GetLength(0);
            foreach(T[,] rotation in rotations)
               if (Util.Matches<T>(dataX, dataY, data, rotation))
                  return new Tuple<int, T[,]>(i, rotation);
         }
         return new Tuple<int, T[,]>(-1, null);
      }

      //------------------------------------------------------------------
      // Adapted from answer on
      // https://stackoverflow.com/questions/42250539/c-sharp-find-2d-small-array-in-2d-big-array
      // Segment factored out from FindPoint. Returns whether the given
      // position in the large matrix matches the small matrix.
      // Useful for rotations and other matching.
      //------------------------------------------------------------------
      public static bool Matches<T>(int dataX, int dataY, T[,] data, T[,] find)
      {
         int dataLen = data.Length;
         int findLen = find.Length;
         bool okay = true;
         for (int j = 0; j < findLen && okay; j++)
         {
            int findX = j % find.GetLength(1);
            int findY = j / find.GetLength(1);

            int checkedX = findX + dataX;
            int checkedY = findY + dataY;

            okay = data[Mod(dataY + findY, data.GetLength(0)), Mod(dataX + findX, data.GetLength(1))].Equals(find[findY, findX]);
         }
         if (okay)
            return true;
         return false;
      }

      //------------------------------------------------------------------
      // Adapted from answer on
      // https://stackoverflow.com/questions/18034805/rotate-mn-matrix-90-degrees
      // Rotates an NxM matrix 90 degrees, used for checking every
      // orientation of an Organism's pattern.
      //------------------------------------------------------------------
      public static T[,] RotateMatrixCounterClockwise<T>(T[,] oldMatrix)
      {
         T[,] newMatrix = new T[oldMatrix.GetLength(1), oldMatrix.GetLength(0)];
         int newColumn, newRow = 0;
         for (int oldColumn = oldMatrix.GetLength(1) - 1; oldColumn >= 0; oldColumn--)
         {
            newColumn = 0;
            for (int oldRow = 0; oldRow < oldMatrix.GetLength(0); oldRow++)
            {
               newMatrix[newRow, newColumn] = oldMatrix[oldRow, oldColumn];
               newColumn++;
            }
            newRow++;
         }
         return newMatrix;
      }

      //------------------------------------------------------------------
      // Generates all possible rotations for any given array
      //------------------------------------------------------------------
      private static T[][,] GenerateRotations<T>(T[,] arr)
      {
         T[][,] rotations = new T[4][,];
         rotations[0] = arr;
         for(int r = 1; r < 4; r++)
            rotations[r] = RotateMatrixCounterClockwise<T>(rotations[r - 1]);
         return rotations;
      }

      //------------------------------------------------------------------
      // Checks whether a found array within data is still valid after
      // being padded.
      //------------------------------------------------------------------
      public static bool IsValidPadding<T>(int startPoint, T[,] data, T[,] find, T padVal)
      {
         //int x = Mod(startPoint % data.GetLength(0) - 1, data.GetLength(0));
         //int y = Mod(startPoint / data.GetLength(0) - 1, data.GetLength(1));

         int x = startPoint % data.GetLength(0) - 1;
         int y = startPoint / data.GetLength(0) - 1;
         return Matches<T>(x, y, data, Util.Pad<T>(find, padVal));
      }

      //------------------------------------------------------------------
      // Performs a modulos operation including negative numbers
      //------------------------------------------------------------------
      public static int Mod(int num, int max)
      {
         return (int)(num - max * Math.Floor((double)num / max));
      }

      //------------------------------------------------------------------
      // Pads the outside of an array with padVal values, e.g.
      // 0000
      // 0XX0  Padding a 1x2 with zeroes.
      // 0000
      //------------------------------------------------------------------
      public static T[,] Pad<T>(T[,] grid, T padVal)
      {
         int xLength = grid.GetLength(1) + 2;
         int yLength = grid.GetLength(0) + 2;
         T[,] paddedGrid = new T[yLength, xLength];
         for (int i = 0; i < paddedGrid.GetLength(1); i++)
         {
            paddedGrid[0, i] = padVal;
            paddedGrid[paddedGrid.GetLength(0) - 1, i] = padVal;
         }
         for (int i = 0; i < paddedGrid.GetLength(0); i++)
         {
            paddedGrid[i, 0] = padVal;
            paddedGrid[i, paddedGrid.GetLength(1) - 1] = padVal;
         }
         for (int i = 1; i < paddedGrid.GetLength(0) - 1; i++)
            for (int j = 1; j < paddedGrid.GetLength(1) - 1; j++)
               paddedGrid[i, j] = grid[i - 1, j - 1];
         return paddedGrid;
      }

      //------------------------------------------------------------------
      // Takes in an array and returns a copy of it.
      //------------------------------------------------------------------
      public static T[,] CopyArray<T>(T[,] arr)
      {
         T[,] ret = new T[arr.GetLength(0), arr.GetLength(1)];
         for (int i = 0; i < ret.GetLength(1); i++)
            for (int j = 0; j < ret.GetLength(0); j++)
               ret[i, j] = arr[i, j];
         return ret;
      }
   }
}
