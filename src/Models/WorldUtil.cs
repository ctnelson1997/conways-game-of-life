﻿//---------------------------------------------------------------------
// Group:   Pioneers
// Project: Conway's Game of Life
// Purpose: Saves and loads script/grid spaces.
//---------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civilization.Models
{
   // --------------------------------------------------------------
   // Controls the saving and loading of scripts onto and off the
   // grid space.
   // --------------------------------------------------------------
   class WorldUtil
   {
      //private const string DIR_PATH = @"..\..\saves\";
      public const string FILE_EXT = @".cgol";

      // --------------------------------------------------------------
      // Loads in a script to the grid based on the filePath
      // --------------------------------------------------------------
      public static void Load(string filePath)
      {
         if (filePath == null || filePath == "")
            return;
         Cartographer.instance.Clear();
         string[] content = File.ReadAllLines(filePath);
         int fileLineLength = content[0].Length;
         int gridLength = Cartographer.instance.max_x_tiles;
         int length = fileLineLength > gridLength ? gridLength : fileLineLength;
         for (int i = 0; i < length; i++)
         {
            char[] line = content[i].ToCharArray();
            for (int j = 0; j < length; j++)
               Cartographer.instance.map[i][j].cell.state = (line[j] == '0' ? Cell.State.DEAD : Cell.State.ALIVE);
         }
      }

      // --------------------------------------------------------------
      // Saves the grid as a script that can be loaded using the load
      // functionality.
      // --------------------------------------------------------------
      public static void Save(string filePath)
      {
         if (filePath == null || filePath == "")
            return;
         int xLength = Cartographer.instance.max_x_tiles;
         int yLength = Cartographer.instance.max_y_tiles;
         string content = "";
         for (int i = 0; i < xLength; i++)
         {
            for (int j = 0; j < yLength; j++)
               content += (Cartographer.instance.map[i][j].cell.state.Equals(Cell.State.ALIVE) ? "1" : "0");
            content += "\n";
         }
         File.WriteAllText(filePath, content);
      }
   }
}
