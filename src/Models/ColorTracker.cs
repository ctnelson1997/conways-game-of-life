﻿//---------------------------------------------------------------------
// Group:   Pioneers
// Project: Conway's Game of Life
// Purpose: Keep track of cells with colors
//---------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Civilization.Models
{
   // -------------------------------------------------------------------
   // Responsible for keeping track of all color changes so that
   // we do not need to skip through the entire map to reset colors.
   // -------------------------------------------------------------------
   class ColorTracker
   {

      #region Singleton Properties
      private static ColorTracker _instance = new ColorTracker();
      public static ColorTracker instance
      {
         get
         {
            return _instance;
         }
      }

      private ColorTracker() { /* ¯\_(ツ)_/¯ */ }
      #endregion
      private List<Tuple<Cell, Brush>> tuples = new List<Tuple<Cell, Brush>>();

      // ----------------------------------------------------------------
      // Adds a cell to the tracker with a color
      // ----------------------------------------------------------------
      public void Add(Cell cell, Brush color)
      {
         tuples.Add(new Tuple<Cell, Brush>(cell, color));
      }

      // ----------------------------------------------------------------
      // Removes all elements from the color tracker
      // ----------------------------------------------------------------
      public void Reset()
      {
         tuples.Clear();
      }

      // ----------------------------------------------------------------
      // Gives each cell their respective color
      // ----------------------------------------------------------------
      public void Paint()
      {
         foreach (Tuple<Cell, Brush> tuple in tuples)
         {
            Cell cell = tuple.Item1;
            cell.color = tuple.Item2;
            cell.tile.ForceUpdate();
         }
      }

      // ----------------------------------------------------------------
      // Resets the cells to their respective color
      // ----------------------------------------------------------------
      public void DoClear()
      {
         foreach (Tuple<Cell, Brush> tuple in tuples)
         {
            Cell cell = tuple.Item1;
            tuple.Item1.ResetColor();
            cell.tile.ForceUpdate();
         }
      }
   }
}
