﻿//---------------------------------------------------------------------
// Group:   Pioneers
// Project: Conway's Game of Life
// Purpose: Updates tile adjacencies via coding and algorithms.
//---------------------------------------------------------------------
using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.ComponentModel;
using Civilization.CommandPattern;

namespace Civilization.Models
{
    // -------------------------------------------------------------------
    // Keeps a list of tiles and updates tile adjacencies.
    // -------------------------------------------------------------------
    public class Register : INotifyPropertyChanged
    {

        #region Singleton Properties
        private static Register _instance = new Register();
        public static Register instance
        {
            get
            {
                return _instance;
            }
        }

        // ----------------------------------------------------------------
        // Prepares the dictionary
        // ----------------------------------------------------------------
        private Register()
        {
            tiles = new List<Tile>();
            aliveOrganisms = new Dictionary<Organism, int>();
            foreach (Organism organism in Organism.ORGANISMS)
                aliveOrganisms.Add(organism, 0);
            gameMode = 0;
            trackColors = false;
            numGliders = 0;
            numBlinkers = 0;
            numBeehives = 0;
        }
        #endregion

        #region INotifyPropertyChangedData
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void NotifyPropertyChanged(string str)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(str));
            }
        }
        #endregion

        public List<Tile> tiles { get; private set; }
        public Dictionary<Organism, int> aliveOrganisms { get; private set; }

        private const int MAX_RAND_GEN = 1000;
        private const int MAX_RAND_Y = 10;

        // Represents whether we are in standard or wireworld
        public int gameMode { get; private set; }

        public bool trackColors { get; private set; }
        public int popCount { get; set; }

        private int _numGliders;
        public int numGliders
        {
            get
            {
                return _numGliders;
            }
            set
            {
                _numGliders = value;
                NotifyPropertyChanged("numGliders");
            }
        }

        private int _numBeehives;
        public int numBeehives
        {
            get
            {
                return _numBeehives;
            }
            set
            {
                _numBeehives = value;
                NotifyPropertyChanged("numBeehives");
            }
        }

        private int _numBlinkers;
        public int numBlinkers
        {
            get
            {
                return _numBlinkers;
            }
            set
            {
                _numBlinkers = value;
                NotifyPropertyChanged("numBlinkers");
            }
        }

        private int _numCells = 0;

        public int numCells
        {
            get
            {
                return _numCells;
            }

            private set
            {
                this._numCells = value;
                NotifyPropertyChanged("numCells");
            }
        }

        // ----------------------------------------------------------------
        // Counts the number of cells
        // ----------------------------------------------------------------
        public void CountCells()
        {
            int cells = 0;
            foreach (Tile tile in tiles)
                if (tile.cell.state.Equals(Cell.State.ALIVE)) cells++;
            numCells = cells;
        }

        // ----------------------------------------------------------------
        // Creates a grid predefined with certain cells.
        // ----------------------------------------------------------------
        public void Seed(int seednum)
        {
            Random rand = new Random(seednum);

            for (int i = 5; i < tiles.Count - 5; i++)
            {
                if (i % seednum == 0)
                {
                    tiles[i].cell.state = Cell.State.ALIVE;
                    tiles[i + rand.Next() % 5].cell.state = Cell.State.ALIVE;
                    tiles[i - rand.Next() % 5].cell.state = Cell.State.ALIVE;
                }
            }
        }

        // ----------------------------------------------------------------
        // Decides which internal update to call
        // ----------------------------------------------------------------
        public void Update()
        {
            //Cartographer.instance.GenerateBoolMap();
            CommandUpdate command = new CommandUpdate(Cartographer.instance.GetStateMap());
            CommandInvoker.instance.Call(command);
            //Cartographer.instance.GenerateBoolMap();
            command.SetPostEffect(Cartographer.instance.GetStateMap());
        }

      // ----------------------------------------------------------------
      // Updates tracking depending are whether tracking is disabled or
      // not.
      // ----------------------------------------------------------------
      public void CommandUpdate()
        {
            if (trackColors)
                this.UpdateTracking();
            else
                this.UpdateNoTracking();
            this.CountCells();
        }

        // ----------------------------------------------------------------
        // Does the logic behind each tick, advancing the generation
        // and updating the lists and tables
        // ----------------------------------------------------------------
        private void UpdateNoTracking()
        {
            this.AdvanceGeneration();
        }

        // ----------------------------------------------------------------
        // Does the logic behind each tick, advancing the generation
        // and updating the lists and tables, WITH COLORS
        // ----------------------------------------------------------------
        private void UpdateTracking()
        {
            ColorTracker.instance.DoClear();
            ColorTracker.instance.Reset();
            this.UpdateNoTracking();
            Cartographer.instance.GenerateBoolMap();
            if (gameMode == 0)
                this.IdentifyOrganisms();
            ColorTracker.instance.Paint();
            this.UpdateNumbers();
        }

      // ----------------------------------------------------------------
      // Updates the various information on the UI that changes every
      // tick.
      // ----------------------------------------------------------------
      public void UpdateUI()
        {
            ColorTracker.instance.DoClear();
            ColorTracker.instance.Reset();
            Cartographer.instance.GenerateBoolMap();
            if (gameMode == 0)
                this.IdentifyOrganisms();
            ColorTracker.instance.Paint();
            this.UpdateNumbers();
        }

        // ----------------------------------------------------------------
        // Updates numbers with dictionary
        // ----------------------------------------------------------------
        public void UpdateNumbers()
        {
            numGliders = aliveOrganisms[Organism.GLIDER];
            numBlinkers = aliveOrganisms[Organism.BLINKER];
            numBeehives = aliveOrganisms[Organism.BEEHIVE];
        }

        // ----------------------------------------------------------------
        // Updates grid from tile adjacencies via algorithms according to
        //    thr rules of Conway's Game of Life. 
        // ----------------------------------------------------------------
        private void AdvanceGeneration()
        {
            popCount = 0;

            foreach (Tile tile in tiles)
                tile.UpdateStatus();

            foreach (Tile tile in tiles)
            {
                tile.Finish();
                if (tile.cell.state.Equals(Cell.State.ALIVE))
                    popCount++;
            }

         NotifyPropertyChanged("popCount");
      }

      // ----------------------------------------------------------------
      // Resets the population
      // ----------------------------------------------------------------
      public void ResetPopulation()
      {
         popCount = 0;
         NotifyPropertyChanged("popCount");
      }

      // ----------------------------------------------------------------
      // Identifies each organism, marking where they appear on the
      // screen. Also gets the total number of organisms
      // ----------------------------------------------------------------
      public void IdentifyOrganisms()
        {
            foreach (Organism organism in Organism.ORGANISMS)
                aliveOrganisms[organism] = organism.Identify();
        }

        // ----------------------------------------------------------------
        // Turns color tracking on or off
        // ----------------------------------------------------------------
        public void ToggleColorTracking()
        {
            ColorTracker.instance.DoClear();
            ColorTracker.instance.Reset();
            trackColors = !trackColors;
        }

        // ----------------------------------------------------------------
        // Switches the gamemode to Conway's Game of Life.
        // ----------------------------------------------------------------
        public void ChangeGameModeToCGOL()
        {
            gameMode = 0;
        }

        // ----------------------------------------------------------------
        // Switches the gamemode to Wire World
        // ----------------------------------------------------------------
        public void ChangeGameModeToWW()
        {
            gameMode = 1;
        }

        // ----------------------------------------------------------------
        // Switches the gamemode to High Life
        // ----------------------------------------------------------------
        public void ChangeGameModeToHL()
        {
            gameMode = 2;
        }

        // ----------------------------------------------------------------
        // Switches the gamemode to the value taken in
        // ----------------------------------------------------------------
        public void ChangeGameMode(int GameM)
        {
            switch (GameM)
            {
                case 0:
                    gameMode = 0;
                    break;
                case 1:
                    gameMode = 1;
                    break;
                case 2:
                    gameMode = 2;
                    break;
                case 3:
                    gameMode = 3;
                    break;
            }
      }

        private Thread section1thread;
        private Thread section2thread, section3thread, section4thread;

        private Section section1 = new Section(0, 50, 0, 50);
        private Section section2 = new Section(0, 50, 50, 100);
        private Section section3 = new Section(50, 100, 0, 50);
        private Section section4 = new Section(50, 100, 50, 100);

        // ----------------------------------------------------------------
        // Advances generation using different threads
        // ----------------------------------------------------------------
        public void Advance()
        {
            section1thread = new Thread(new ThreadStart(section1.AdvanceSection));
            section1thread.Start();
            section2thread = new Thread(new ThreadStart(section2.AdvanceSection));
            section2thread.Start();
            section3thread = new Thread(new ThreadStart(section3.AdvanceSection));
            section3thread.Start();
            section4thread = new Thread(new ThreadStart(section4.AdvanceSection));
            section4thread.Start();
        }


        // ----------------------------------------------------------------
        // Advances generation using different threads
        // ----------------------------------------------------------------
        private struct Section
        {
            private int yMin, yMax, xMin, xMax;
            public Section(int y_Min, int y_Max, int x_Min, int x_Max)
            {
                yMin = y_Min;
                yMax = y_Max;
                xMin = x_Min;
                xMax = x_Max;
            }

            // ----------------------------------------------------------------
            // Advances generation using different threads
            // ----------------------------------------------------------------
            public void AdvanceSection()
            {
                for (int i = yMin; i <= yMax; i++)
                {
                    for (int lcv = xMin; lcv < xMax; lcv++)
                    {
                        Tile t = Cartographer.instance.map[i][lcv];
                        t.UpdateStatus();
                    }
                }

                for (int i = yMin; i <= yMax; i++)
                {
                    for (int lcv = xMin; lcv < xMax; lcv++)
                    {
                        Tile t = Cartographer.instance.map[i][lcv];
                        t.Finish();
                    }
                }
            }
        }
    }
}
