﻿//---------------------------------------------------------------------
// Group:   Pioneers
// Project: Conway's Game of Life
// Purpose: Keep a 2D map of tiles and do their respective operations.
//          Does the generation and assignment of tiles. Re-aligns the
//          map to the window size.
//---------------------------------------------------------------------

using System;
using System.Threading;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Diagnostics;
using System.Windows;
using Civilization.CommandPattern;

namespace Civilization.Models
{
   // -------------------------------------------------------------------
   // Keep a 2D map of tiles and do their respective operations.
   // Does the generation and assignment of tiles. Re-aligns the
   // map to the window size.
   // -------------------------------------------------------------------
   public class Cartographer : INotifyPropertyChanged
   {
      #region INotifyPropertyChangedData
      public event PropertyChangedEventHandler PropertyChanged;

      public static event PropertyChangedEventHandler PropertyChanged2;

      protected void OnPropertyChanged(string propertyName)
      {
         var handler = PropertyChanged2;

         if (handler != null)
         {
            handler(this, new PropertyChangedEventArgs(propertyName));
         }
      }

      private void NotifyPropertyChanged(string str)
      {
         if (PropertyChanged2 != null)
         {
            PropertyChanged2(this, new PropertyChangedEventArgs(str));
         }
      }

      private static void NotifyPropertyChanged2(string str)
      {
         if (PropertyChanged2 != null)
         {
            PropertyChanged2(Cartographer.instance.map, new PropertyChangedEventArgs(str));
         }
      }
      #endregion

      public ObservableCollection<ObservableCollection<Tile>> map { get; set; }
      public bool[,] boolMap { get; private set; }

      private const int DEFAULT_X_AXIS_LENGTH = 100;
      private const int DEFAULT_Y_AXIS_LENGTH = 100;

      //private const int NUMBER_OF_X_TILES = 100;

      // Gets and sets grid_size
      public int grid_size { get; set; }
      //gets and (privately) sets x_axis_length
      public double x_axis_length { get; private set; }
      //Gets and (privately) sets y_axis_length
      public double y_axis_length { get; private set; }
      //Gets and (privately) sets max_x_tiles
      public int max_x_tiles { get; private set; }
      //Gets and (privately) sets max_y_tiles
      public int max_y_tiles { get; private set; }
      //Gets and (privately) sets tile_size
      public int tile_size { get; private set; }

      #region Singleton Properties
      private static Cartographer _instance = new Cartographer();
      public static Cartographer instance
      {
         get
         {
            return _instance;
         }
      }

      private Cartographer() { /* ¯\_(ツ)_/¯ */ }
      #endregion

      // ----------------------------------------------------------------
      // Aligns tiles with the proper size on the x and y axis
      // ----------------------------------------------------------------
      public void SetWindow(double x_axis_length, double y_axis_length)
      {
         if (grid_size == 0) // testing
            grid_size = DEFAULT_X_AXIS_LENGTH;
            
         this.x_axis_length = x_axis_length;
         this.y_axis_length = y_axis_length;
         tile_size = (int) x_axis_length / grid_size;
         max_x_tiles = (int) x_axis_length / tile_size;
         max_y_tiles = (int) x_axis_length / tile_size;
         Reset();
      }

      // ----------------------------------------------------------------
      // Resets the map in the background, silently.
      // ----------------------------------------------------------------
      public void Reset()
      {
         GenerateMap();
         AssignTiles();
      }

      // ----------------------------------------------------------
      // Generates a boolean map for quicker calculations
      // ----------------------------------------------------------
      public void GenerateBoolMap()
      {
         for(int i = 0; i < Cartographer.instance.max_x_tiles; i++)
            for (int j = 0; j < Cartographer.instance.max_y_tiles; j++)
               boolMap[i, j] = (Cartographer.instance.map[i][j].cell.state.Equals(Cell.State.ALIVE) ? true : false);
      }

      // ----------------------------------------------------------
      // Gets the cells state from each tile and tranfers them into
      //    a new array.
      // ----------------------------------------------------------
      public Cell.State[,] GetStateMap()
      {
         Cell.State[,] stateMap = new Cell.State[Cartographer.instance.max_x_tiles, Cartographer.instance.max_y_tiles];
         for (int i = 0; i < Cartographer.instance.max_x_tiles; i++)
            for (int j = 0; j < Cartographer.instance.max_y_tiles; j++)
               stateMap[i, j] = (Cell.State) ((byte) Cartographer.instance.map[i][j].cell.state); // copies
         return stateMap;
      }

      // ----------------------------------------------------------------
      // Sets all the cells to dead, updating the screen as it does
      // ----------------------------------------------------------------
      public void Clear()
      {
         Register.instance.ResetPopulation();
         CommandInvoker.instance.Clear();
         for (int y = 0; y < max_y_tiles; y++)
            for (int x = 0; x < max_x_tiles; x++)
               map[y][x].cell.state = Cell.State.DEAD;
      }

      // ----------------------------------------------------------------
      // Sets the cell states of the grid to the previous generation.
      // ----------------------------------------------------------------
      public void Revert(Cell.State[,] states)
      {
         for (int i = 0; i < states.GetLength(1); i++)
            for (int j = 0; j < states.GetLength(0); j++)
               Cartographer.instance.map[i][j].cell.state = states[i,j];
         Register.instance.UpdateUI();
      }

      // ----------------------------------------------------------------
      // Generates a 2D array of ObservableCollections, assigning
      // tiles to each in accordance with the size
      // ----------------------------------------------------------------
      private void GenerateMap()
      {
         boolMap = new bool[Cartographer.instance.max_x_tiles, Cartographer.instance.max_y_tiles];
         map = new ObservableCollection<ObservableCollection<Tile>>();
         /*for (int y = 0; y <= max_y_tiles; y++)
         {
            map.Add(new ObservableCollection<Tile>());
            ThreadXColumn txc = new ThreadXColumn(y, max_x_tiles);
            Thread t = new Thread(new ThreadStart(txc.addXtiles));
            t.Start();
         }*/
         for (int y = 0; y < max_y_tiles; y++)
         {
            map.Add(new ObservableCollection<Tile>());
            for (int x = 0; x < max_x_tiles; x++)
            {
               Tile t = new Tile(x, y);
               map[y].Add(t);
               Register.instance.tiles.Add(t);
            }
               
         }
      }  

      // ----------------------------------------------------------------
      // Assigns adjacencies for all tiles in accordance to the
      // 0 1 2   array on the left. Constants for magic numbers
      // 3 X 4   would be pointless here. Assigns tile adjacencies
      // 5 6 7   from top-down, left-right. Null if non-existent.
      // ----------------------------------------------------------------
      private void AssignTiles()
      {
         
         for (int y = 0; y < max_y_tiles; y++)
            for (int x = 0; x < max_x_tiles; x++)
            {
               map[y][x].adjacencies = new List<Tile>(8);
               List<Tile> adjacencies = map[y][x].adjacencies;
               adjacencies.Add(map[Util.Mod(y - 1,max_y_tiles)][Util.Mod(x - 1, max_x_tiles)]);                    
               adjacencies.Add(map[Util.Mod(y - 1, max_y_tiles)][Util.Mod(x, max_x_tiles)]);
               adjacencies.Add(map[Util.Mod(y - 1, max_y_tiles)][Util.Mod(x + 1, max_x_tiles)]);
               adjacencies.Add(map[Util.Mod(y, max_y_tiles)][Util.Mod(x - 1, max_x_tiles)]);
               adjacencies.Add(map[Util.Mod(y, max_y_tiles)][Util.Mod(x + 1, max_x_tiles)]);
               adjacencies.Add(map[Util.Mod(y + 1, max_y_tiles)][Util.Mod(x - 1, max_x_tiles)]);
               adjacencies.Add(map[Util.Mod(y + 1, max_y_tiles)][Util.Mod(x, max_x_tiles)]);
               adjacencies.Add(map[Util.Mod(y + 1, max_y_tiles)][Util.Mod(x + 1, max_x_tiles)]);

               //ThreadXColumn txc = new ThreadXColumn(y, max_x_tiles);
               //Thread t = new Thread(new ThreadStart(txc.assignTiles));
               //t.Start();
            }
      }

      // ----------------------------------------------------------------
      // Old code for using threads to speed up grid construction.
      //    Might be useful for later.
      // ----------------------------------------------------------------



      /*public class ThreadXColumn
      {
         private int yval;
         private int max_x_column;
         private int max_y_row;
         public ThreadXColumn(int y, int max_x, int max_y = 0)
         {
            yval = y;
            max_x_column = max_x;
            max_y_row = max_y;
         }

         public void addXtiles()
         {
            for (int x = 0; x < max_x_column; x++)
               Cartographer._instance.map[yval].Add(new Tile(x, yval));
         }

         public void assignTiles()
         {
            List<Tile> temp_List = new List<Tile>(8);
            for (int x = 0; x < max_x_column; x++)
            {
               for (int i = 0; i < 8; i++)
                  temp_List.Add(null);

               if (x - 1 >= 0 && yval - 1 >= 0)
                  temp_List[0] = Cartographer._instance.map[yval - 1][x - 1];

               if (yval - 1 >= 0)
                  temp_List[1] = Cartographer._instance.map[yval - 1][x];

               if (x + 1 < max_x_column && yval - 1 >= 0)
                  temp_List[2] = Cartographer._instance.map[yval - 1][x + 1];

               if (x + 1 < max_x_column)
                  temp_List[3] = Cartographer._instance.map[yval][x + 1];

               if (x + 1 < max_x_column && yval + 1 < max_y_row)
                  temp_List[4] = Cartographer._instance.map[yval + 1][x + 1];

               if (yval + 1 < max_y_row)
                  temp_List[5] = Cartographer._instance.map[yval + 1][x];

               if (x - 1 >= 0 && yval + 1 < max_y_row)
                  temp_List[6] = Cartographer._instance.map[yval + 1][x - 1];

               if (x - 1 >= 0)
                  temp_List[7] = Cartographer._instance.map[yval][x - 1];

               Cartographer._instance.map[yval][x].adjacencies = temp_List;

               temp_List.RemoveRange(0, temp_List.Count);
            }
         }
      }*/


   }


}
