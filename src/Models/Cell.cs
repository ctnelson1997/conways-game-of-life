﻿//---------------------------------------------------------------------
// Group:   Pioneers
// Project: Conway's Game of Life
// Purpose: Keeps track of the state, lifetime, and mobility of
//          a cell. Each cell also has a specific color which
//          is currently only white.
//---------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Civilization.Models
{

   // -------------------------------------------------------------------
   // Keeps track of the state, lifetime, and mobility of
   // a cell. Each cell also has a specific color which
   // is currently only white.
   // -------------------------------------------------------------------
   public class Cell : INotifyPropertyChanged
   {

      #region INotifyPropertyChangedData
      public event PropertyChangedEventHandler PropertyChanged;

      protected void OnPropertyChanged(string propertyName)
      {
         var handler = PropertyChanged;

         if (handler != null)
         {
            handler(this, new PropertyChangedEventArgs(propertyName));
         }
      }

      public void NotifyPropertyChanged(string str)
      {
         if (PropertyChanged != null)
         {
            PropertyChanged(this, new PropertyChangedEventArgs(str));
         }
      }

      #endregion

      // ----------------------------------------------------------------
      // English representation of state, now only alive and dead.
      // ----------------------------------------------------------------
      public enum State : byte
      {
         //OBLIVION,
         DEAD,
         ALIVE,
         CONDUCTOR,   //new
         ELECTRONHEAD, //new
         ELECTRONTAIL,  //new
         WALL
      }

      //Each Cell has a tile
      public Tile tile { get; set; }

      //Cell is printed this color
      private Brush _color;

      //Gets and sets the _color while notifying
      //the GUI that the cell's color changed
      public Brush color
      {
         get
         {
            return _color;
         }
         set
         {
            this._color = value;
            NotifyPropertyChanged("color");
         }
      }

      //Cell status
      private State _state;

      public bool changeNext { get; set; }

      // ----------------------------------------------------------------
      // Always updates the color and UI with the change in state.
      // ----------------------------------------------------------------
      public State state
      {
         set
         {
            this._state = value;
            switch (this._state)
            {
               //case State.OBLIVION:
               //   this.color = Palette.TRANSPARENT;
               //   break;
               case State.DEAD:
                  this.color = Palette.TRANSPARENT;
                  break;
               case State.ALIVE:
                  this.color = Palette.WHITE;
                  break;
               case State.CONDUCTOR:
                  this.color = Palette.GOLDENROD;
                  break;
               case State.ELECTRONHEAD:
                  this.color = Palette.BLUE;
                  break;
               case State.ELECTRONTAIL:
                  this.color = Palette.RED;
                  break;
               case State.WALL:
                  this.color = Palette.DARK_black;
                  break;
            }

            NotifyPropertyChanged("color");
         }
         get
         {
            return this._state;
         }
      }

      // ----------------------------------------------------------------
      // Changes the state depending on the gamemode
      // ----------------------------------------------------------------
      internal void ChangeStatus()
      {
         if (Register.instance.gameMode == 0)
         {
            switch (this._state)
            {
               case State.ALIVE:
                  this.state = State.DEAD;
                  break;
               case State.DEAD:
                  this.state = State.ALIVE;
                  break;
            }
         }
         else if (Register.instance.gameMode == 1)
         {
            switch (this._state)
            {
               case State.DEAD:
                  this.state = State.CONDUCTOR;
                  break;
               case State.ELECTRONHEAD:
                  this.state = State.ELECTRONTAIL;
                  break;
               case State.ELECTRONTAIL:
                  this.state = State.CONDUCTOR;
                  break;
               case State.CONDUCTOR:
                  this.state = State.ELECTRONHEAD;
                  break;
            }
         }
         else if (Register.instance.gameMode == 2)
         {
            switch (this._state)
            {
               case State.ALIVE:
                  this.state = State.DEAD;
                  break;
               case State.DEAD:
                  this.state = State.ALIVE;
                  break;
            }
         }
         else if (Register.instance.gameMode == 3) //CGOL WC
         {
            switch (this._state)
            {
               case State.ALIVE:
                  this.state = State.WALL;
                  break;
               case State.DEAD:
                  this.state = State.ALIVE;
                  break;
               case State.WALL:
                  this.state = State.ALIVE;
                  break;
            }
         }
      }

      // ----------------------------------------------------------------
      // Resets the color implicittly by changing the state
      // ----------------------------------------------------------------
      public void ResetColor()
      {
         switch (this._state)
         {
            case State.ELECTRONHEAD:
               this.state = State.DEAD;
               break;
            case State.ELECTRONTAIL:
               this.state = State.DEAD;
               break;
            case State.CONDUCTOR:
               this.state = State.DEAD;
               break;
            case State.ALIVE:
               this.state = State.ALIVE;
               break;
            case State.DEAD:
               this.state = State.DEAD;
               break;
            case State.WALL:
               this.state = State.DEAD;
               break;
         }
      }

      // ----------------------------------------------------------------
      // Makes tile state transparent.
      // ----------------------------------------------------------------
      public void ChangeStateDead()
      {
         switch (this._state)
         {
            case State.ELECTRONHEAD:
               this.state = State.DEAD;
               break;
            case State.ELECTRONTAIL:
               this.state = State.DEAD;
               break;
            case State.CONDUCTOR:
               this.state = State.DEAD;
               break;
            case State.ALIVE:
               this.state = State.DEAD;
               break;
            case State.DEAD:
               this.state = State.DEAD;
               break;
            case State.WALL:
               this.state = State.DEAD;
               break;
         }
      }

      public int lifetime { get; private set; }
      // ----------------------------------------------------------------
      // Constructs a cell with a given state
      // ----------------------------------------------------------------
      public Cell(State state)
      {
         this.changeNext = false;
         this.state = state;
      }

      // ----------------------------------------------------------------
      // Adds an iteration to the cell's lifetime
      // ----------------------------------------------------------------
      public void IncrementLifetime()
      {
         this.lifetime++;
      }
   }
}
