﻿//---------------------------------------------------------------------
// Group:   Pioneers
// Project: Conway's Game of Life
// Purpose: Non-functional requirement. You may only use these
//          colors. Colors may be added if they agree with the
//          overall color scheme
//---------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Civilization.Models
{
   // -------------------------------------------------------------------
   // Non-functional requirement. You may only use these
   // colors. Colors may be added if they agree with the
   // overall color scheme
   // -------------------------------------------------------------------
   public static class Palette
   {
      public static Brush DARK_BLACK = Brushes.Black;
      public static Brush LIGHT_BLACK = Brushes.GhostWhite;
      public static Brush GRAY = Brushes.Gray;
      public static Brush WHITE = Brushes.White;
      public static Brush BLUE = Brushes.Blue;
      public static Brush GREEN = Brushes.Green;
      public static Brush TRANSPARENT = Brushes.Transparent;
      public static Brush RED = Brushes.Red;
      public static Brush GOLDENROD = Brushes.Goldenrod;
      public static Brush DARK_black = Brushes.Black;
   }
}
