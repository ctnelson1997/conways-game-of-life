﻿//---------------------------------------------------------------------
// Group:   Pioneers
// Project: Conway's Game of Life
// Purpose: Controls the timer and ticks of the game.
//---------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;



namespace Civilization.Models
{
    // -------------------------------------------------------------------
    // Creates and controls the timers and ticks of the game.
    // -------------------------------------------------------------------
    public class Scheduler : INotifyPropertyChanged
    {
        #region Singleton Properties
        private static Scheduler _instance = new Scheduler();
        public static Scheduler instance
        {
            get
            {
                return _instance;
            }
        }

        private Scheduler()
        {
            time = 0;
            generation_Count = time.ToString();
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 0, 0, 1);
            timer.Tick += OnTick;
        }
        #endregion
        #region INotifyPropertyChangedData
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void NotifyPropertyChanged(string str)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(str));
            }
        }
        #endregion
        private DispatcherTimer timer;
        public int time { get; private set; }
        public string generation_Count { get; set; }

        public int gameSpeedRepresentation { get; set; }

        private bool move = false;

        // ----------------------------------------------------------------
        // Method that is called every tick.
        // ----------------------------------------------------------------
        public void OnTick(object sender, EventArgs e)
        {
            if (move)
                Register.instance.Update();
            time++;
            generation_Count = time.ToString();
            NotifyPropertyChanged("generation_Count");
        }

        // ----------------------------------------------------------------
        // Pauses the timer.
        // ----------------------------------------------------------------
        public void Pause()
        {
            move = false;
            timer.Stop();
        }

        // ----------------------------------------------------------------
        // Resumes the timer.
        // ----------------------------------------------------------------
        public void Resume()
        {
            move = true;
            timer.Start();
        }

        // ----------------------------------------------------------------
        // Toggles the timer.
        // ----------------------------------------------------------------
        public void ToggleState()
        {
            if (move)
                this.Pause();
            else
                this.Resume();
        }

        // ----------------------------------------------------------------
        // Changes the speed of the timer.
        // ----------------------------------------------------------------
        public void ChangeSpeed(int speed)
        {
            timer.Interval = new TimeSpan(0, 0, 0, 0, speed);
        }

        // ----------------------------------------------------------------
        // Resets the on-screen timer.
        // ----------------------------------------------------------------
        public void ResetGenerations()
        {
            time = 0;
            NotifyPropertyChanged("time");
        }

      // ----------------------------------------------------------------
      // Decrements generation number
      // ----------------------------------------------------------------
      public void DecrementGenerations()
        {
            time--;
            generation_Count = time.ToString();
            NotifyPropertyChanged("generation_Count");
        }

      // ----------------------------------------------------------------
      // Increments generation number
      // ----------------------------------------------------------------
      public void IncrementGenerations()
        {
            time++;
            generation_Count = time.ToString();
            NotifyPropertyChanged("generation_Count");
        }
    }
}
